package com.xa.day1;

public class Main {

    public static void main(String[] args) {
       garisBiodata();
       identitasDiri(3374113008990005l, "Muhammad Ekananda Alfandza Fajar", "Kab. Semarang, 30-08-1999", "Pria", "Jl. Urip Sumoharjo Perumahan Griya Sinar Mutiara Blok I2");
       
    }

    private static void garisBiodata() {
        // Template Biodata
        System.out.println("---------------------------------------------");
        System.out.println("------------------Biodata--------------------");
        System.out.println("---------------------------------------------");
    }

    private static void identitasDiri(long nik, String nama, String ttl, String gender, String alamat) {
        // Cetak Biodata
        // Variabel myVar = new Variabel();
        System.out.println("NIK              : " + nik);
        System.out.println("Nama             : " + nama);
        System.out.println("Tempat/Tgl lahir : " + ttl);
        System.out.println("Jenis Kelamin    : " + gender);
        System.out.println("Alamat           : " + alamat);
    } 

}