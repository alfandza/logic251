package com.xa.day9;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Scanner;

public class javaSoalString {
    
    public static void main(String[] args) {
        String replay = "Y";
        Scanner scan = new Scanner(System.in);
        while(replay.equals("Y")) {
            System.out.print("\nMasukkan nomor soal : ");
            int exam = scan.nextInt();
            switch (exam) {
                case 1:
                    System.out.println("\nSoal nomor " + exam);
                    soalSatu(scan);
                    break;
                case 2:
                    System.out.println("\nSoal nomor " + exam);
                    soalDua(scan);
                    break;
                case 3:
                    System.out.println("\nSoal nomor " + exam);
                    soalTiga(scan);
                    break;
                case 4:
                    System.out.println("\nSoal nomor " + exam);
                    soalEmpat(scan);
                    break;
                case 5:
                    System.out.println("\nSoal nomor " + exam);
                    soalLima(scan);
                    break;
                case 6:
                    System.out.println("\nSoal nomor " + exam);
                    soalEnam(scan);
                    break;
                case 7:
                    System.out.println("\nSoal nomor " + exam);
                    soalTujuh();
                    break;
                case 8:
                    System.out.println("\nSoal nomor " + exam);
                    soalDelapan(scan);
                    break;
                case 9:
                    System.out.println("\nSoal nomor " + exam);
                    soalSembilan(scan);
                    break;
                case 10:
                    System.out.println("\nSoal nomor " + exam);
                    soalSepuluh(scan);
                    break;
                default:
                    System.out.println("\nSoal tidak tersedia");
                }
            // scan.nextLine();
            System.out.print("\nMau lagi ? (Y/N)");
            replay = scan.nextLine().toUpperCase();
            }
    }

    public static void soalSatu(Scanner scan) {
        System.out.println("camelCase Separator");
        System.out.print("\nMasukkan input : ");
        scan.nextLine();
        String camelCase = scan.nextLine();
        ArrayList <String> camelCaseSeparator = new ArrayList<String>();
        //Need learning more
        for (String temp : camelCase.split("(?<=[a-z])(?=[A-Z])")) {
         camelCaseSeparator.add(temp);
        }
        System.out.println(camelCaseSeparator);
        System.out.println(camelCaseSeparator.size());

    }

    public static void soalDua(Scanner scan) {
        System.out.println("Password Checker");
        System.out.print("Masukkan input : ");
        scan.nextLine();
        String input = scan.nextLine();
        String specialChar = "!@#S%^&*()-+";
        int level = 0;
        System.out.println("Soal Nomer 2");
        System.out.println("Input Password : " + input);
        
        //Char array for specialChar
        char[] specialCharArry = new char[specialChar.length()];
        for(int i=0;i<specialChar.length();i++) {
            specialCharArry[i] = specialChar.charAt(i);
        }

        //check if password at least 6

        if (input.length() > 5) level++;

        //Check Special Character
        for (int i=0;i<input.length();i++){
            char temp = input.charAt(i);
            for (int j=0; j<specialCharArry.length;j++) {
                char check = specialCharArry[j];
                if (temp == check) {
                    level++;
                    break;
                }
            }
        }

        //Check angka 
        for (int i=0;i<input.length();i++){
            char temp = input.charAt(i);
            if (temp >= 48 && temp <=57) {
                level++;
                break;
            }
        }
        
        //Check lowercase
        for (int i=0;i<input.length();i++){
            char temp = input.charAt(i);
            if (temp >= 97 && temp <=122) {
                level++;
                break;
            }
        }
        
        //Check Uppercase
        for (int i=0;i<input.length();i++){
            char temp = input.charAt(i);
            if (temp >= 65 && temp <=90) {
                level++;
                break;
            }
        }


        System.out.println("Your Password level is : " + level);

    }

    public static void soalTiga(Scanner scan) {
        System.out.println("Caesar Cipher");
        System.out.print("Masukkan input : ");
        scan.nextLine();
        String input = scan.nextLine();
        System.out.print("Masukkan pergeseran alphabet : ");
        int alphaOffside = scan.nextInt();
        System.out.println("\nInput : " + input);
        System.out.println("Alphabet offside : " + alphaOffside);
        // int alphaOffside=2;
        // String input = "middle-Outz";
        String output = "";
        for (int i=0; i<input.length();i++){
            char temp = input.charAt(i);
            boolean test = Character.isLetter(temp);
            for (int j=0;j<alphaOffside;j++){
                if (test == false) break;
                // if (temp > 64 && temp<91 && temp>96 && temp<123)
                if (temp == 90) {
                    temp = 65;
                    continue;
                }
                if (temp == 122) {
                    temp = 97;
                    continue;
                }
                temp++;
            }
            output += Character.toString(temp);
        }
        System.out.println("Output : "+output);
    }

    public static void soalEmpat(Scanner scan) {
        System.out.println("Mars Exploraton");
        String send = "SOSSPSS";
        int inputSignal = 4;
        String expect = "SOS";
        String expectedSignal = "";
        String differ = "x"; // Ada beda, keluar X
        String findDif = ""; 
        int countDif = 0;
        for (int i=0;i<inputSignal;i++){
            expectedSignal += expect;
        }
        System.out.println("Expected Signal : " + expectedSignal);
        System.out.println("Signal sended \t: " + send);

        //Case if sending too much
        if (send.length() > expectedSignal.length()){
            System.out.println("Too much signal! Please enter the right SOS as much as " + inputSignal + " times");
            return;
        }

        //Normal case
        System.out.print("Differences \t: ");
        for (int i=0; i<send.length();i++){
            char temp = send.charAt(i);
            char check = expectedSignal.charAt(i);
            if (check == temp){
                System.out.print(" ");
            } else {
                System.out.print(differ);
                countDif++;
            }
        }

        // Case if send too less than expectedSignal
        if (send.length() < expectedSignal.length()){
            int howMuch = expectedSignal.length() - send.length();
            for (int i=0;i<howMuch;i++){
                findDif += differ;
                countDif++;
            }
            System.out.print(findDif);
        }
        
        System.out.println("\nDifference Signal : " + countDif);
    }

    public static void soalLima(Scanner scan) {
        System.out.println("hackerrank in a String!");
        String check = "hackerrank";
        System.out.print("Masukkan input : ");
        scan.nextLine();
        String input = scan.nextLine();

        char[] charCheck = check.toCharArray();
        Arrays.sort(charCheck);
        String sortedCheck = new String(charCheck);
        System.out.println(sortedCheck);

        char[] charInput = input.toCharArray();
        Arrays.sort(charInput);
        String sortedInput = new String(charInput);
        System.out.println(sortedInput);


        int hitung = 0;
        for (int i=0;i<sortedInput.length();i++){
            if (hitung < sortedInput.length() && sortedInput.charAt(i) == sortedCheck.charAt(hitung)) 
            hitung++;
        }

        if (hitung == sortedCheck.length()) System.out.println("\nYES!");
        else System.out.println("\nNO");

    }

    public static void soalEnam(Scanner scan) {
        System.out.println("Pangram Checker");
        System.out.print("Masukkan kalimat yang akan diperiksa untuk Pangram : ");
        scan.nextLine();
        String input = scan.nextLine();
        // String input = "The quick brown fox jumps over the lazy dog";
        if (input.length()<26) {System.out.println("Kurang dari 26"); return;}
        String inputLow = input.toLowerCase();
        // String[] inputArry = inputLow.split(" ");
        // System.out.println(Arrays.toString(inputArry));
        boolean[] check = new boolean[26];
        int index = 0;
        for (int i=0;i<inputLow.length();i++){
            char temp = input.charAt(i);
            if (temp >= 'a' && temp <= 'z'){
                index = temp - 'a';
                check[index] = true;
            } else continue;
        }
        // System.out.println(Arrays.toString(check));
        int truecheck=0;
        for (int i=0;i<check.length;i++){
            if (check[i] == true) truecheck++;
        }
        System.out.println("Input : " + input);
        if (truecheck == 26) System.out.println("Ini Pangram");
        else System.out.println("Ini bukan pangram");
        
    }

    public static void soalTujuh() {
        String angka = "910111213";
        System.out.println("Input : " + angka);
        ArrayList <String> angkaSeparator = new ArrayList<String>();
        //Need learning more
        for (String temp : angka.split("(?<=[0-9])|(?<=[1-9])(?=[0-9])")) {
         angkaSeparator.add(temp);
        }
        System.out.println("Output : " + angkaSeparator);
    }

    public static void soalDelapan(Scanner scan) {
        System.out.println("Gemstones check");
        System.out.print("Masukkan input 1 : ");
        scan.nextLine();
        String input1 = scan.nextLine();
        System.out.print("Masukkan input 2 : ");
        String input2 = scan.nextLine();
        System.out.print("Masukkan input 3 : "); 
        String input3 = scan.nextLine();
        System.out.println("Input");
        System.out.println(input1);
        System.out.println(input2);
        System.out.println(input3);

        HashSet <Character> input1Array = new HashSet<>();
        HashSet <Character> input2Array = new HashSet<>();
        HashSet <Character> input3Array = new HashSet<>();

        for (int i=0;i<input1.length();i++) input1Array.add(input1.charAt(i));
        for (int i=0;i<input2.length();i++) input2Array.add(input2.charAt(i));
        for (int i=0;i<input3.length();i++) input3Array.add(input3.charAt(i));

        input1Array.retainAll(input2Array);
        input1Array.retainAll(input3Array);
        System.out.println("\nOutput");
        System.out.println(input1Array);

    }

    public static void soalSembilan(Scanner scan) {
        System.out.println("Anagram checker");
        System.out.print("Masukkan input pertama : ");
        scan.nextLine();
        String inputA = scan.nextLine();
        System.out.print("Masukkan input kedua : ");
        String inputB = scan.nextLine();
        System.out.println("Input");
        System.out.println(inputA);
        System.out.println(inputB);

        HashSet<Character> setInputA = fillHashSet(inputA);
        HashSet<Character> setInputB = fillHashSet(inputB);
        
        setInputA.retainAll(setInputB);
        int delChar1 = inputA.length() - setInputA.size();
        int delChar2 = inputB.length() - setInputA.size();
        int delGab = delChar1 + delChar2;
        
        if (delGab == 0) System.out.println("Anagram");
        else System.out.println("Bukan anagram, hapus " + delGab + " karakter terlebih dahulu");
        
    }

    public static void soalSepuluh(Scanner scan) {
        System.out.println("Two Strings");
        System.out.print("Masukkan inputan 1 : ");
        scan.nextLine();
        String input1 = scan.nextLine();
        System.out.print("Masukkan inputan 2 : ");
        String input2 = scan.nextLine();

        HashSet<Character> input1Set = fillHashSet(input1);
        HashSet<Character> input2Set = fillHashSet(input2);

        input1Set.retainAll(input2Set);
        System.out.println(input1Set);
        System.out.println(input1Set.size());
    }

    public static HashSet<Character> fillHashSet(String input) {
        HashSet<Character> hashSet = new HashSet<>();
        for (int i=0; i<input.length(); i++) {
            hashSet.add(input.charAt(i));
        }
        return hashSet;
    }

}
