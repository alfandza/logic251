package com.xa.extra.SoalHari10.Soal5OOP;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Scanner;

public class Main {
    public static void main (String[] args){
        Scanner scan = new Scanner(System.in);
        ArrayList<Laptop> listLaptop = new ArrayList<>();
        ArrayList<Mouse> listMouse = new ArrayList<>();

        System.out.println("Masukan budget");
        int inputBudget = scan.nextInt();

        Pembeli wildan = new Pembeli("Wildan", inputBudget);
        Laptop lap1 = new Laptop("Notebook MacBook Pro i7 2012", 9999990);
        Laptop lap2 = new Laptop("Notebook Samsung series 9 i7", 9850000);
        Laptop lap3 = new Laptop("Notebook Asus e43j", 7250000);
        Mouse mos1 = new Mouse("Mouse Logitec Gaming", 750000);
        Mouse mos2 = new Mouse("Mouse Logitec M220", 210000);
        Mouse mos3 = new Mouse("Mouse Logitec m185", 100000);

        listLaptop.add(lap1);
        listLaptop.add(lap2);
        listLaptop.add(lap3);
        listMouse.add(mos1);
        listMouse.add(mos2);
        listMouse.add(mos3);

        int [] temp = new int[listLaptop.size()*listMouse.size()];
        String [] daftarBeli = new String[listLaptop.size()*listMouse.size()];
        HashMap<Integer, String> daftar = new HashMap<>();
        int idx =0;
        for(Laptop i: listLaptop){
            for (Mouse j:listMouse){
                daftarBeli[idx] = i.getNama()+" dan "+j.getNama();
                temp[idx] = wildan.beli(i,j);
                daftar.put(temp[idx], daftarBeli[idx]);
                wildan.setUang(inputBudget);
                idx++;
            }
        }

        ArrayList<Integer> daftarBiaya = new ArrayList<>();

        for (int t: temp){
            //System.out.print(t+" ");
            if(t > 0)
                daftarBiaya.add(t);
        }

        //System.out.println();
       // for (int b: daftarBiaya)
        //    System.out.print(b+" ");

        //System.out.println();
        int min = Collections.min(daftarBiaya);
        System.out.println("belanja mouse dan laptop terboros dengan sisa: Rp."+min);
        int totalBelanja = wildan.getUang() -min;
        System.out.println("Total uang dihabiskan "+totalBelanja);
        System.out.println();
        System.out.println("yang dibelanjakan adalah: ");
        System.out.println(daftar.get(min));

    }

}
