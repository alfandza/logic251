package com.xa.latihanLogic07;

import java.lang.reflect.Array;
import java.util.*;

public class soalLogic07 {
 
    public static void main(String[] args) {
        String replay = "Y";
        int exam = 0;

        while (replay.equals("Y")){
            Scanner scanner = new Scanner(System.in);

            System.out.print("\nMasukkan no Soal (1-9) : ");
            exam = scanner.nextInt();

            switch (exam){
                case 1:
                    System.out.println("\nSoal nomer 1");
                    soalSatu(scanner);
                    break;
                case 2:
                    System.out.println("\nSoal nomer 2");
                    soalDua(scanner);
                    // System.out.println();
                    // soalDua(1,1,5);
                    break;
                case 3:
                    System.out.println("\nSoal nomer 3");
                    soalTiga(scanner);
                    break;
                case 4:
                    System.out.println("\nSoal nomer 4");
                    soalEmpat(scanner);
                    break;
                case 5:
                    System.out.println("\nSoal nomer 5");
                    soalLima(scanner);
                    break;
                case 6:
                    System.out.println("\nSoal nomer 6");
                    soalEnam(scanner);
                    break;
                case 7:
                    System.out.println("\nSoal nomer 7");
                    soalTujuh(1,7,3);
                    System.out.println("");
                    soalTujuh(1,5,4);
                    break;
                case 8:
                    System.out.println("\nSoal nomer 8");
                    soalDelapan(7,3);
                    System.out.println();
                    soalDelapan(5,4);
                    break;
                case 9:
                    System.out.println("\nSoal nomer terakhir");
                    soalTerakhir(4);
                    break;
                default:
                    System.out.println("Nomor tidak tersedia");
            }

        }
    }

    public static void soalSatu(Scanner scanner) {
        System.out.print("\nMasukkan panjang deret : ");
        int n = scanner.nextInt();
        int nilaiAwal = 9;
        int konstantaKurang = 2;
        // int nilaiAwal=9,konstantaKurang=2,n=7;
        ArrayList <Integer> output = new ArrayList<Integer>();
        
        for (int i=0; i<n; i++){
            output.add(nilaiAwal);
            nilaiAwal-=konstantaKurang;
        }
        
        System.out.println(output);

    }

    public static void soalDua(Scanner scanner) {
        int nilaiAwal = 1;
        int konstantaTambah = 1;
        System.out.print("\nMasukkan panjang deret : ");
        int n = scanner.nextInt();
        int[] urutArray = new int[n];
        
        for (int i=0;i<n;i++){
            urutArray[i]=nilaiAwal;
            nilaiAwal+=konstantaTambah;
            konstantaTambah+=1;
            System.out.print(urutArray[i] + " ");
        }
        System.out.println();
    }

    public static void soalTiga(Scanner scanner) {
        long nilaiAwal = 1l;
        System.out.print("\nMasukkan jumlah deret : ");
        int n = scanner.nextInt();
        ArrayList <Long> output = new ArrayList<Long>();
        
        for (int i=0; i<n; i++){
            output.add(nilaiAwal);
            nilaiAwal*=n;
        }
        
        System.out.println(output);
    }

    public static void soalEmpat(Scanner scanner) {
        int nilaiAwal=0;
        System.out.print("\nMasukkan panjang deret : ");
        int column = scanner.nextInt();
        // int konstantaTambah=1;
        System.out.print("\nMasukkan panjang baris : ");
        int row = scanner.nextInt();
        int[][] urutArray = new int[row][column];

        for (int i=0;i< column; i++) {
            for (int j = 0; j < row; j++) {
                urutArray[j][i] = nilaiAwal;
                nilaiAwal++;
            }
        }

        //cara repot
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < column; j++) {
                System.out.print(urutArray[i][j] + "\t");
            }
            System.out.println();
        }

        // cara cepat
        System.out.println(Arrays.deepToString(urutArray));

        //cara normal
        for (int i = 0; i<row;i++){
            System.out.println(Arrays.toString(urutArray[i]));
        }


    }

    public static void soalLima(Scanner scanner) {
        int nilaiAwal=0;
        int baris = 2;
        // int konstantaTambah=1;
        System.out.print("\nMasukkan angka ganjil : ");
        int n = scanner.nextInt();

        // Case ketika angka yang dimasukkan genap, akan keluar dari method
        if (n % 2 == 0){
            System.out.print("\nAngka yang dimasukkan genap !");
            return;
        }

        int lanjut=(n/2)+1;
        int[][] urutArray = new int[baris][n];

        for (int i=0;i<n;i++){
            for (int j=0;j<baris;j++){
                urutArray[j][i] = nilaiAwal;
                if (nilaiAwal == n) break;
                nilaiAwal++;
            }
            if (nilaiAwal == n) break;
        }

        int nilaiLoncat = nilaiAwal-2;
        for (int i=lanjut;i<n;i++){
            for (int j=baris-1;j>=0;j--){
                urutArray[j][i] = nilaiLoncat;
                nilaiLoncat--;
            }
        }
        
        for (int i=0;i<2;i++){
            System.out.println(Arrays.toString(urutArray[i]));
        }
    }

    public static void soalEnam(Scanner scanner) {
        System.out.print("\nMasukkan panjang deret (Ganjil dan > 2) : ");
        int n = scanner.nextInt();
        final int konstanta = 7;
        int test = n / 2;
        int penampung = konstanta*test;
        int[][] nilaiArray = new int[2][n];

        // cek ganjil atau genap
        if (n % 2 == 1 && n > 2) {
            //cek nilai n/2 genap atau ganjil
            if (test % 2 == 0) {
                for (int i=0; i<n;i++){
                    nilaiArray[1][i]=penampung;
                    penampung--;
                }
            } else {
                for (int i=n-1; i>=0;i--){
                    nilaiArray[1][i]=penampung;
                    penampung++;
                }
            }
        } else {
            System.out.println("Angka salah!");
            return;
        }
        
        for (int i=0; i<n; i++){
            nilaiArray[0][i] = i+1;
            System.out.print(nilaiArray[0][i] + "\t");
        }
        System.out.println();

        for (int i=0;i<n;i++){
            System.out.print(nilaiArray[1][i] + "\t");
        }
        System.out.println();

        //Beda case, yang diatas case kelipatan 7
        // int n = 7;
        // int baris = 4;
        // int nilai = 1;
        // int total = 0;
        // int[][] soalArray = new int[baris][n];
        // boolean add = true;

        // for (int i=0; i<baris;i++){
            
        //     for (int j=0; j<n;j++){
        //         total += nilai;
        //         if (add == true){
        //             soalArray[i][j] = nilai++;
        //         } else soalArray[i][j] = nilai--;
        //     }

        //     nilai = total-1;
        //     total = 0;

        //     if (add) {
        //         add = false;
        //     } else add = true;
        // }

        // for (int i=0;i<baris;i++){
        //     System.out.println(Arrays.toString(soalArray[i]));
        // }

    }

    public static void soalTujuh(int nilaiAwal, int n1, int n2) {
        // int nilaiAwal = 1; Nilai awalan pada array [0][0]
        // int n1=7 Nilai awalan pada array [1][0], serta konstanta perkalian tetap
        // n2=3; konstanta penjumlahan

        int[][] nilaiArray = new int[2][n1];

        for (int i=0;i<n1; i++) {
            nilaiArray[0][i] = nilaiAwal;
            nilaiAwal += n2;
            System.out.print(nilaiArray[0][i] + "\t");
        }
        System.out.println();
        for (int i=0;i<n1;i++) {
            nilaiArray[1][i] = n1*nilaiArray[0][i];
            System.out.print(nilaiArray[1][i] + "\t");
        }
        System.out.println();
    }

    public static void soalDelapan(int n1, int n2) {
        // int n1 = 7, n2 = 3; // n1 adalah panjang deret, n2 adalah dimana dia tukar tempat
        int temp=1; // penampunt untuk array [0]
        int[][] nilaiArray = new int[2][n1];
        // isi array index [0][i]
        for (int i=0; i<n1; i++){
            if (temp % n2 == 0) {
                nilaiArray[1][i] = temp;
                temp++;
                continue;
            }
            nilaiArray[0][i] = temp;
            temp++;
        }
        // isi array index [1][i]
        int temp2=n2; // penampung untuk array [1]
        int test=n2*n2; // cek untuk array [1]
        for (int i=0;i<n1;i++){
            if (temp2 % test == 0) {
                nilaiArray[0][i]=temp2;
                temp2+=n2;
                continue;
            }
            nilaiArray[1][i] = temp2;
            temp2+=n2;
        }

        // print array
        for (int i=0;i<2;i++){
            for (int j=0; j<n1; j++){
                System.out.print(nilaiArray[i][j] + "\t");
            }
            System.out.println();
        }


    }

    public static void soalTerakhir(int n) {
        int temp = 1;
        int soalArray[][] = new int[n][n];
        for (int i=0;i<n;i++){
            for (int j=0;j<n;j++){
                soalArray[i][j] = temp;
                temp++;
                System.out.print(soalArray[i][j] + "\t");
            }
            System.out.println();
        }

    }

}