//soal hari ke 6

package com.xa.latihanSoalBaru;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.*;

public class extraLatihan04 {
    
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        // soalSatu(scan);
        // soalDua(scan);
        // soalTiga(scan);
        // soalLima(scan);
        // soalEnam(scan);
        soalTujuh(scan);
        // soalDelapan(scan);
    }

    public static void soalSatu(Scanner scan) {
                //cetak pertanyaan
                System.out.println("Seorang anak main game online di sebuah warnet dengan durasi sewa 3 jam di mulai " +
                "dari pukul 8.15 pagi dengan harga sewa Rp. 3.500 /-jam. \n" +
                "   a. Pada pukul berapa anak tersebut selesai dan berapa biayanya ? \n" +
                "   b. Anak tersebut memperpanjang durasi selama 2 jam, maka berapakah harga keseluruhannya dan selesai pada jam berapa ? \n\n");
   
               //harga dalam hitungan menit
               double price = 3500.0 / 60.0;
   
               //durasi sewa dalam satuan menit
               long durationMinute = 60 * 3;

               DateTimeFormatter format = DateTimeFormatter.ofPattern("H:mm");

               /*Jawaban bagian A*/
               //mencari waktu mulai dan berakhirnya sewa
               LocalTime startTime = LocalTime.parse("8:15",format);
               LocalTime endTime = startTime.plusMinutes(durationMinute);
   
               //mencari biaya sewa
               double cost = Math.round(price * durationMinute);

               System.out.println("Session 1 - pada jam : " + endTime + " dengan biaya Rp. " + cost);

               /*Jawaban bagian B*/
               endTime = endTime.plusHours(2); // nambah 2 jam

               cost = cost + (3500*2);
   
            //    durationMinute += 60 * 2;
            //    durationTime = TimeSpan.FromMinutes(durationMinute);
            //    endTime = startTime + durationTime;
            //    cost = Math.Round(price * durationMinute);
   
               System.out.println("Session 2 - pada jam : " + endTime + " dengan biaya Rp. " + cost);
   
    }

    public static void soalDua(Scanner scan) {
        System.out.println("====EXAM 02====");
        System.out.println("Pada program bootcamp di PT. Xsis Mitra Utama berdurasi 7 minggu dengan rincian :\n" +
        "   - 2 hari sofskill \n" +
        "   - 10 hari logic \n" +
        "   - 1 hari Filtering Test 1 \n" +
        "   - 10 hari Teknologi \n" +
        "   - 1 hari kick of meeting mini project \n" +
        "   - 10 hari mini project \n" +
        "   - 1 hari filtering test 2   \n" +
        "Pada hari apa dan tanggal berapakah filtering 1 dan 2 jika start program bootcamp tanggal 27 November 2019\n\n");

        int dSoftSkill = 2, dLogic = 10, dFT1 = 1, dTeknologi = 10, dKickOffMinPro = 1, dMinPro = 10, dFt2 = 1;


        //total hari sampai Filtering Tes 1

        int totalHariFt1 = dSoftSkill+dLogic;
        int totalHariFt2 = totalHariFt1+dFT1+dTeknologi+dKickOffMinPro+dMinPro;

        // membuat variabel tanggal mulai
        String tglStart = "27/11/2019";
        DateTimeFormatter formatStart = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        DateTimeFormatter formatEnd = DateTimeFormatter.ofPattern("EEEE dd MMMM yyyy");

        LocalDate tanggalMulai = LocalDate.parse(tglStart, formatStart);
        LocalDate tglFT1 = GetDaysWork(tanggalMulai, totalHariFt1);
        System.out.println("Filtering Test 1 pada hari " + tglFT1.format(formatEnd));
        
        LocalDate tglFT2 = GetDaysWork(tglFT1,totalHariFt2);
        System.out.println("Filtering Test 2 pada hari " + tglFT2.format(formatEnd));
    }

    public static void soalTiga(Scanner scan){
        System.out.println("===SOAL 03 cek Jam===");
        System.out.print("Masukkan berangkat : ");
        String berangkat = scan.nextLine();

        System.out.print("Masukkan sampai : ");
        String sampai = scan.nextLine();

        System.out.print("Masukkan kembali : ");
        String kembali = scan.nextLine();
        
        long jarakA = hitungJarakA(sampai, kembali, "jam");
        long jarakAMenit = hitungJarakA(sampai, kembali, "menit")-(jarakA*60);
        System.out.println("Lamanya di Gandaria Mall selama "+jarakA+" jam dan " + jarakAMenit +" menit");

        long jarakB = hitungJarakB(berangkat, kembali, "jam");
        long jarakBMenit = hitungJarakB(berangkat, kembali, "menit")-(jarakB*60);
        System.out.println("Lamanya saya keluar adalah "+jarakB+" jam dan "+jarakBMenit + " menit");

    }

    public static void soalLima(Scanner scan) {
        System.out.println("====SOAL 05====");

        //Input panjang deret
        System.out.print("Masukkan panjang deret :");
        int panjangDeret = scan.nextInt();

        //bikin deret genap dan ganjil
        int[] deretGenap = new int[panjangDeret];
        int[] deretGanjil = new int[panjangDeret];
        int ganjil = 11,genap=10;
        for (int i = 0; i < panjangDeret; i++) {
            deretGenap[i]=genap;
            deretGanjil[i]=ganjil;
            genap+=2;
            ganjil+=2;
        }

        int[] deretJumlah = new int[panjangDeret];
        for(int i=0;i<panjangDeret;i++){
            int temp = deretGenap[i]+deretGanjil[i];
            deretJumlah[i] = temp;
        }
        
        System.out.println("Deret Genap : " + Arrays.toString(deretGenap));
        System.out.println("Deret Ganjil : " + Arrays.toString(deretGanjil));
        System.out.println("Deret Jumlah : " + Arrays.toString(deretJumlah));

    }

    public static void soalEnam(Scanner scan) {
        System.out.println("====SOAL 06====");

        //Input panjang deret
        System.out.print("Masukkan kalimat : ");
        String kalimat = scan.nextLine().toLowerCase();
        char[] sortKalimat = kalimat.toCharArray();
        Arrays.sort(sortKalimat);

        System.out.println("Sort kalimat : " + Arrays.toString(sortKalimat));

        //akses per character di sortKalimat
        String vokal = "", konsonan = "";
        for (char c : sortKalimat) {
            String perKata = String.valueOf(c);

            if(perKata.equals("a") || perKata.equals("i") || perKata.equals("u") || perKata.equals("e") || perKata.equals("o")){
                vokal += perKata;
            }else if (c >= 97 && c <= 122){
                konsonan += perKata;
            }
        }

        System.out.println("Vokal : "+vokal);
        System.out.println("Konsonan : "+konsonan);

    }

    public static void soalTujuh(Scanner scan) {
        System.out.println("====SOAL 07====");

        //Input panjang deret
        System.out.print("Masukkan panjang deret : ");
        int panjangDeret = scan.nextInt();
        int[] bilPrim = bilPrim(panjangDeret);
        int[] bil7 = bil7(panjangDeret);
        int[] bilGab = new int[panjangDeret];

        for (int i=0;i<panjangDeret;i++){
            bilGab[i] = bilPrim[i] + bil7[i];
        }

        System.out.println(Arrays.toString(bilPrim));
        System.out.println(Arrays.toString(bil7));
        System.out.println(Arrays.toString(bilGab));

    }

    public static void soalDelapan(Scanner scan) {
        System.out.println("===SOAL 08 FOR KUPI===");
        System.out.println("Promo diskon For Kupi 50%" +
                            "\ndengan maksimal diskon harga Rp. 30.000 dan minimal pembelian Rp. 70.000" +
                            "\nditambah diskon Grap 10% setelah harga dipotong diskon lainnya dengan maksimal diskon Rp. 10.000."+
                            "\n(harga percup @30.000)");
        //input
        System.out.print("\nMasukkan saldo : ");
        int saldo = scan.nextInt();

        //deklarasi
        double  diskonKopi = 0.50, 
                diskonGrap = 0.10,
                maxDiskonKopi = 30000,
                maxDiskonGrap = 10000, 
                hargaKopiSat = 30000,
                bayar = 0;

        //Check saldo mencukupi
        if (saldo < hargaKopiSat) {
            System.out.println("Saldo anda tidak mencukupi!");
            return;
        }

        // mencari jumlah cup yang bisa didapat
        int jmlCup = (int) (saldo / hargaKopiSat);

        // mencari jumlah bayar
        bayar = hargaKopiSat * jmlCup;

        if (bayar >= 70000){
            //mencari diskon kopi
            double hargaDiskonKopi = bayar*diskonKopi;
            if (hargaDiskonKopi >=maxDiskonKopi) hargaDiskonKopi = 30000;
            bayar = bayar - hargaDiskonKopi;

            //mencari diskon grap
            double hargaDiskonGrap = bayar*diskonGrap;
            if (hargaDiskonGrap >=maxDiskonGrap) hargaDiskonGrap = 10000;
            bayar = bayar - hargaDiskonGrap;
        }

        System.out.println("Jumlah cup kopi adalah : " + jmlCup + " pcs");
        System.out.println("Harga kopi yang harus dibayar adalah : Rp" + bayar);
        System.out.println("Kembalian : " + (saldo-bayar));

    }

    //method
    public static LocalDate GetDaysWork(LocalDate paramdate, int countDays)
        {
            LocalDate result = paramdate;

            for (int i = 1; i <= countDays; i++)
            {
                result = result.plusDays(1);

                DateTimeFormatter dtf = DateTimeFormatter.ofPattern("EEEE");
                String nameOfDays = result.format(dtf);
                if (nameOfDays.contains("Saturday")||nameOfDays.contains("Sunday"))
                {
                    countDays++;
                }
            }

            return result;
        }

    public static long hitungJarakA(String datang, String pulang,String pilihan){
        DateTimeFormatter format = DateTimeFormatter.ofPattern("HH.mm");
        LocalTime datangFormat = LocalTime.parse(datang,format);
        LocalTime pulangFormat = LocalTime.parse(pulang,format);

        long selisihJam = ChronoUnit.HOURS.between(datangFormat, pulangFormat);
        long selisihMenit = ChronoUnit.MINUTES.between(datangFormat, pulangFormat);

        if (pilihan.equals("jam")) return selisihJam;
        else if (pilihan.equals("menit")) return selisihMenit;
        else return selisihJam;
    }

    public static long hitungJarakB(String berangkat, String pulang,String pilihan){
        DateTimeFormatter format = DateTimeFormatter.ofPattern("HH.mm");
        LocalTime berangkatFormat = LocalTime.parse(berangkat,format);
        // LocalTime datangFormat = LocalTime.parse(datang,format);
        LocalTime pulangFormat = LocalTime.parse(pulang,format);

        long selisihJam = ChronoUnit.HOURS.between(berangkatFormat, pulangFormat);
        long selisihMenit = ChronoUnit.MINUTES.between(berangkatFormat, pulangFormat)+8;

        if (pilihan.equals("jam")) return selisihJam;
        else if (pilihan.equals("menit")) return selisihMenit;
        else return selisihJam;
    }

    public static int[] bilPrim(int panjang){
        int[] prim = new int[panjang];
        int loop = 0;
        for (int i=2; loop<panjang;i++){
            int faktor = 0;
            for (int j=1;j<=i;j++){
                if (i%j==0){
                    faktor = faktor+1;
                }
            }
            if (faktor == 2) {
                prim[loop] = i;
                loop++;
            }
        }
        return prim;
    }

    public static int[] bil7(int panjang){
        int[] kel7 = new int[panjang];
        int awal = 7;
        int loop = 0;
        for (int i=1;loop<panjang;i++){
            kel7[loop] = awal*i;
            loop++;
        }
        return kel7;

    }


}
