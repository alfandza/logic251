package com.xa.day8;

// import java.sql.Time;
// import java.text.ParseException;
// import java.text.SimpleDateFormat;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
// import java.util.Date;
import java.util.Scanner;

public class javaWarmUp {
    static String replay = "Y";
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        
        while (replay.equals("Y")){
        System.out.print("Masukkan soal (1-10) :");
        int soal = scan.nextInt();
        switch (soal) {
            case 1:
                System.out.println("Soal 1");
                soalSatu(scan);
                break;
            case 2:
                System.out.println("Soal 2");
                soalDua(scan);
                break;
            case 3:
                System.out.println("Soal 3");
                soalTiga();
                break;
            case 4:
                System.out.println("Soal 4");
                soalEmpat(scan);
                break;
            case 5:
                System.out.println("Soal 5");
                soalLima(scan);
                break;
            case 6:
                System.out.println("Soal 6");
                soalEnam(scan);
                break;
            case 7:
                System.out.println("Soal 7");
                soalTujuh(scan);
                break;
            case 8:
                System.out.println("Soal 8");
                soalDelapan(scan);
                break;
            case 9:
                System.out.println("Soal 9");
                soalSembilan(scan);
                break;
            case 10:
                System.out.println("Soal 10");
                soalSepuluh();
                break;
            default:
                System.out.println("Nomer tidak tersedia!");
         }
        }   
        scan.close();
    }

    public static void soalSatu(Scanner scan) {
        System.out.print("\nMasukkan panjang deret : ");
        int n = scan.nextInt();
        int[] soalArray = new int[n];

        for (int i=0;i<n;i++){
            System.out.print("Masukkan angka :");
            int temp = scan.nextInt();
            soalArray[i]=temp;
        }
        
        int total = 0;

        for (int i = 0; i < soalArray.length; i++) {
            System.out.print(soalArray[i]);
            total += soalArray[i];
            if (i < soalArray.length - 1) {
                System.out.print("+");
            } else
                System.out.print("=" + total);
        }
        System.out.println();
    }

    public static void soalDua(Scanner scan) {
        System.out.print("Masukkan angka untuk diagonal : ");
        int n = scan.nextInt();                                     // nilai N untuk panjang dan baris Array Dua Dimensi
        int[][] diagonalSoal = new int[n][n];
        for (int i=0;i<n;i++){                                      // For untuk memasukkan angka pada Array Dua Dimensi
            for (int j=0;j<n;j++){
                System.out.print("Masukkan angka index " + i + " dan " + j + " : ");
                int temp = scan.nextInt();
                diagonalSoal[i][j] = temp;
            }
        }
        for (int i=0;i<diagonalSoal.length;i++){                    // Hanya untuk print arraynya saja
            System.out.println(Arrays.toString(diagonalSoal[i]));
        }
        System.out.println();
        int sum1 = 0, sum2 = 0;
        
        for (int i = 0; i < diagonalSoal.length; i++) {             //Mulai Pemrosesan Diagonal untuk Summary 1 dan Summary 2
            for (int j = 0; j < diagonalSoal[i].length; j++) {
                if (i == j) sum1 += diagonalSoal[i][j];
                if (i + j == n) sum2 += diagonalSoal[i][j];
            }
        }
        int diff = sum1 - sum2;
        System.out.print("|" + sum1 + "-" + sum2 + "|=" + Math.abs(diff));
        System.out.println();
    }

    public static void soalTiga() {
        int[] plusMinus = { -4, 3, -9, 0, 4, 1 };
        double jmlPos = 0, jmlNeg = 0, jmlZero = 0;
        int totalLength = plusMinus.length;
        for (int i = 0; i < totalLength; i++) {
            int temp = plusMinus[i];
            if (temp > 0)
                jmlPos++;
            else if (temp == 0)
                jmlZero++;
            else
                jmlNeg++;
        }

        // DecimalFormat df = new DecimalFormat("#0.000000");
        System.out.printf("%,.5f%n",jmlPos / totalLength);
        // System.out.println(new DecimalFormat("#0.000000").format(jmlNeg / totalLength));
        System.out.printf("%,.5f%n",jmlNeg / totalLength);
        System.out.printf("%,.5f%n",jmlZero / totalLength);
    }

    public static void soalEmpat(Scanner scan) {
        System.out.print("Masukkan nilai n panjang dan lebar tangga : ");
        int n = scan.nextInt();
        String pager = "#";
        String space = " ";
        int diffPrint = n - 1;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (i + j >= diffPrint)
                    System.out.print(pager);
                else
                    System.out.print(space);
            }
            System.out.println();
        }

    }

    public static void soalLima(Scanner scan) {
        try {
            System.out.print("\nMasukkan Deret dengan menggunakan , : ");
            String input = scan.nextLine();
            String[] inputArray = input.split(",");
            int[] miniMaxSumArray = convArrayStringToArrayInt(inputArray);
            Arrays.sort(miniMaxSumArray);
            System.out.println(Arrays.toString(miniMaxSumArray));
            int miniSum = 0, maxSum = 0;

            for (int i = 0; i < miniMaxSumArray.length; i++) {
                if (i<4) miniSum += miniMaxSumArray[i];
                if (i>=miniMaxSumArray.length-4) maxSum += miniMaxSumArray[i];
            }

            System.out.println(miniSum + " " + maxSum);
        } catch (Exception e) {
            if (e.getMessage().contains("For input")) {
                System.out.println("Error! gunakan separator (,) bukan separator lain!");
            }
        }
    }

    public static int[] convArrayStringToArrayInt(String[] inArray){
        int[] miniMaxSumArray = new int[inArray.length];
        for (int i=0;i<inArray.length;i++){
            int temp = Integer.parseInt(inArray[i]);
            miniMaxSumArray[i] = temp;
        }
        return miniMaxSumArray;
    }

    public static void soalEnam(Scanner scan) {
        System.out.print("Masukkan umur untuk menentukan jumlah lilin yana akan ditiup : ");
        int n = scan.nextInt();
        int[] candleTall = new int[n];
        
        for (int i=0;i<n;i++){
            System.out.print("Masukkan panjang lilin " + (i+1) + " : ");
            int temp = scan.nextInt();
            candleTall[i] = temp;
        }
        System.out.println(Arrays.toString(candleTall));
        int max = candleTall[0];
        int countMax = 0;
        for (int i = 0; i < candleTall.length; i++) {
            int temp = candleTall[i];
            if (temp > max) temp = max;
            else if (temp == max) countMax++;
        }

        System.out.println(countMax);
    }

    public static void soalTujuh(Scanner scan){
        System.out.print("Masukkan format timer dengan hh:mm:ssa (Contoh : 05:32:00AM) : ");
        String input=scan.nextLine();
        DateTimeFormatter tInput = DateTimeFormatter.ofPattern("hh:mm:ssa");
        DateTimeFormatter tOutput = DateTimeFormatter.ofPattern("HH:mm:ss");

        String tConv = LocalTime.parse(input,tInput).format(tOutput);
        System.out.println(tConv);
        
    }

    public static void soalDelapan(Scanner scan) {
        System.out.print("Masukkan kata : ");
        String input = scan.nextLine();
        System.out.print("Masukkan pergeseran Alphabet : ");
        int alphaGeser= scan.nextInt();
        System.out.println("\nInput : " + input);
        System.out.println("Alphabet offside : " + alphaGeser);
        
        String output = "";
        for (int i=0; i<input.length();i++){
            char temp = input.charAt(i);
            boolean test = Character.isLetter(temp);
            for (int j=0;j<alphaGeser;j++){
                if (test == false) break; //Selain alphabet, keluar dari loop
                // if (temp > 64 && temp<91 && temp>96 && temp<123)
                if (temp == 90) {
                    temp = 65;
                    continue;
                }
                if (temp == 122) {
                    temp = 97;
                    continue;
                }
                temp++;
            }
            output += Character.toString(temp);
        }
        System.out.println("Output : "+output);
    }

    public static void soalSembilan(Scanner scan) {
        System.out.print("Masukkan Password :");
        String input = scan.nextLine();
        // String specChar = "!@#S%^&*()-+";
        int level = 0;
        int detectKap = 0,detectKec=0,detectAngka=0,detectSpecChar=0;
        System.out.println("Input Password : " + input);
        
        //String array for specialChar
        char[] specCharArry = {'!','@','#','$','%','^','&','*','(',')','-','+'};


        for (int i=0;i<input.length();i++){
            char temp = input.charAt(i);

            if (temp >= 48 && temp <= 57) detectAngka=1; //angka
            else if (temp >= 97 && temp <= 122) detectKec=1; // lowercase
            else if (temp >= 65 && temp <= 90) detectKap=1; //uppercase
            else detectSpecChar=1;
        }

        // // Check angka
        // for (int i = 0; i < input.length(); i++) {
        //     char temp = input.charAt(i);
        //     if (temp >= 48 && temp <= 57) {
        //         level++;
        //         break;
        //     }
        // }

        // // Check lowercase
        // for (int i = 0; i < input.length(); i++) {
        //     char temp = input.charAt(i);
        //     if (temp >= 97 && temp <= 122) {
        //         level++;
        //         break;
        //     }
        // }

        // // Check Uppercase
        // for (int i = 0; i < input.length(); i++) {
        //     char temp = input.charAt(i);
        //     if (temp >= 65 && temp <= 90) {
        //         level++;
        //         break;
        //     }
        // }

        // // Check Special Character
        // for (int i = 0; i < input.length(); i++) {
        //     char temp = input.charAt(i);
        //     if (temp >= specCharArry[0] && temp < specCharArry[11]) {
        //         level++;
        //         break;
        //     }
        // }
        level = detectAngka+detectKap+detectKec+detectSpecChar;
        System.out.println("Your Password level is : " + level);
        // switch (level) {
        //     case 1:
        //         System.out.println("Your Password level is : " + level);
        //         break;
        //     case 2:
        //         System.out.println("Your Password level is : " + level);
        //         break;
        //     case 3:
        //         System.out.println("Your Password level is : " + level);
        //         break;
        //     case 4:
        //         System.out.println("Your Password level is : " + level);
        //         break;
        //     default:
        //         System.out.println("Error!");
        //         break;
        // }

    }

    public static void soalSepuluh() {
        String send = "SOSSPSSQSSORSOS";
        int inputSignal = 5;
        String expect = "SOS";
        String expectedSignal = "";
        int success = 0;
        int error =0;
        for (int i=0;i<inputSignal;i++){
            expectedSignal += expect;
        }

        System.out.println("Expected Signal : " + expectedSignal);
        System.out.println("Signal sended \t: " + send);

        for (int i=2;i<send.length();i+=3){
            if (send.charAt(i) == expectedSignal.charAt(i) && send.charAt(i-1) == expectedSignal.charAt(i-1) && send.charAt(i-2) == expectedSignal.charAt(i-2)){
                success++;
            }else{
                error++;
            }
        }
        
        System.out.println("Fail : " + error);
        System.out.println("Success : " + success);
        System.out.println("Signal : " + inputSignal);
    }

}
