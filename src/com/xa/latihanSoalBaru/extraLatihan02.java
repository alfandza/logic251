//file soal hari ke 5

package com.xa.latihanSoalBaru;

import java.util.Scanner;

public class extraLatihan02 {
    
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        // soalEmpat(scan);
        // soalLima(scan);
    }

    public static void soalSatu(Scanner scan) {
        System.out.println("KONVERSI GALON, TEKO DAN BOTOL");
        double galonToTeko = 6, tekoToBotol = 3, botolToGelas = 2.5;
        System.out.print("Masukkan yang mau dikonversi : ");
        String inputAir = scan.nextLine();
        System.out.print("Masukkan input isi : ");
        int input = scan.nextInt();
        System.out.print("Mau diconvert jadi apa ? ");
        scan.nextLine();
        String inputConv = scan.nextLine();
        


    }

    public static void soalEmpat(Scanner scan) {
        System.out.println("===SOAL 04 CARI NILAI TERTINGGI===");
        System.out.print("Masukkan deret nilai : ");
        String[] in = scan.nextLine().split(" ");
        cariTertinggi(in);
    }

    public static void soalLima(Scanner scan) {
        System.out.println("===SOAL 05 MENCARI KPK===");
        System.out.print("Masukkan bilangan yang mau dicari KPK : ");
        int in = scan.nextInt();
        cariKPK(in);
    }

    //Method

    public static void cariTertinggi(String[] input){
        int count = 0;
        int big = Integer.parseInt(input[0]);
        for (int i=0;i<input.length;i++){
            int temp = Integer.parseInt(input[i]);
            if (big < temp) big = temp;
        }

        for (int i=0;i<input.length;i++){
            int temp = Integer.parseInt(input[i]);
            if (temp == big) count++;
        }

        System.out.println("Bilangan tertinggi = " + big);
        System.out.println("Jumlahnya sebanyak = " + count);
    }

    public static void cariKPK(int input){
        System.out.print("\nBilangannya adalah : ");
        for (int i=1;i<=50;i++){
            if (input % i == 0) System.out.print(i + " ");
        }
        System.out.println();
    }



}
