//latihan hari ke 10

package com.xa.latihanSoalBaru;

// import java.util.HashMap;
// import java.util.Map;
import java.util.Scanner;

public class extraLatihan03 {
    
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        // soalSatu(scan);
        // soalDua(scan);
        // soalTiga(scan);
        // soalEmpat();
        soalLima();
    }

    public static void soalSatu(Scanner scan) {
        System.out.println("===SOAL 01 Sensor Tengah===");
        System.out.print("Masukkan input : ");
        String[] input = scan.nextLine().split(" ");
        censorTime(input);

    }

    public static void soalDua(Scanner scan) {
        System.out.println("===Soal 02 Jumlah Deret===");
        System.out.print("Masukkan angka penjumlah : ");
        int jumlah = scan.nextInt();
        System.out.print("\nMasukkan angka deret : ");
        Scanner input = new Scanner(System.in);
        String[] angkaDeret = input.nextLine().split(" ");
        minMaxDeret(angkaDeret, jumlah);
        input.close();
    }

    public static void soalTiga(Scanner scan){
        System.out.println("===SOAL 03 ; checker===");
        System.out.print("Berapa banyak kalimat yang mau dimasukkan ?");
        int n = scan.nextInt();
        String[] input = new String[n];
        Scanner inp = new Scanner(System.in);
        for (int i=0;i<n;i++){
            System.out.print("Input kata : ");
            String in = inp.nextLine();
            input[i] = in;
        }
        for (int i=0;i<n;i++){
            if (input[i].contains(";")) System.out.println(input[i]);
        }
    }

    public static void soalEmpat() {
        
    }
    
    public static void soalLima() {
        String[][] hargaLaptop = hargaLaptop();
        String[][] hargaMouse = hargaMouse();
        int max = 10000000;
        int checkHarga = 0;
        int checkM = -1, checkL=-1;
        for (int i=0;i<3;i++){
            int temp = Integer.parseInt(hargaLaptop[1][i]);
            for (int j=0;j<hargaMouse.length;j++){
                int temp2 = Integer.parseInt(hargaMouse[1][j]);
                checkHarga = temp + temp2;
                if (checkHarga < max){
                    checkM = j;
                    checkL = i;
                }
                if (checkM >= 0 && checkL >= 0){
                    System.out.println("Yang bisa dibeli adalah " + hargaLaptop[0][checkL]+" dan "+hargaMouse[0][checkM]);
                }
            }
        }

    }



    //method
    public static void censorTime(String[] input){
        for (int i=0;i<input.length;i++){
            for (int j=0;j<input[i].length();j++){
                char temp = input[i].charAt(j);
                if (j == 0 || j == input[i].length()-1) System.out.print(temp);
                else if (j == 1) System.out.print("***");
                else continue;
            }
            System.out.print(" ");
        }
    }

    public static void minMaxDeret(String[] input, int angka){
        int min=0,max=0;
        for (int i=0;i<input.length;i++){
            int temp = Integer.parseInt(input[i]);
            if (i == 0) min = angka + temp;
            else if (i == input.length-1) max = angka+temp;
        }
        System.out.println("Min = " + min);
        System.out.println("Max = " + max);
    }

    public static String[][] hargaMouse(){
        String[][] hargaJualM = new String[3][3];
        hargaJualM[0][0]="Mouse Logitec Gaming";
        hargaJualM[0][1]="Mouse Logitec M220";
        hargaJualM[0][2]="Mouse Logitec m185";
        hargaJualM[1][0]="750000";
        hargaJualM[1][1]="210000";
        hargaJualM[1][2]="100000";
        return hargaJualM;
    }

    public static String[][] hargaLaptop(){
        String[][] hargaJualL = new String[2][3];
        hargaJualL[0][0] = "Notebook MacBook Pro i7 2012";
        hargaJualL[0][1] = "Notebook Samsung series 9 i7";
        hargaJualL[0][2] = "Notebook Asus e43j";
        hargaJualL[1][0] = "9999990";
        hargaJualL[1][1] = "9850000";
        hargaJualL[1][2] = "7250000";
        return hargaJualL;
    }

    public static String[][] listFilmA(){
        String[][] listFilm = new String [2][3];
        listFilm[0][0] = "X-Men Dark Poenix";
        listFilm[0][1] = "Haikyuu Session 1";
        listFilm[0][2] = "Friend Zone";
        listFilm[1][0] = "7";
        listFilm[1][1] = "14";
        listFilm[1][2] = "5";
        return listFilm;
    }

}
