--SOAL 1

select * from employees;

alter table employees add column tax numeric (8,2);


--SOAL 2 dan 3
update 	employees 
set 	tax = case  	when employees.salary < 7000 then '0'
						else employees.salary*0.1 
						end

select * from employees;
		
--SOAL 4

select 		e.employee_id,
			concat(e.first_name,' ',e.last_name) as fullname,
			e.salary,
			e.tax,
			e.salary - e.tax as total_gaji
from 		employees e


--soal 5
select 		t1.department_name,
			count(t1.department_id) as total
from 		departments t1
inner join	employees t2 on t1.department_id = t2.department_id
group by	t1.department_name 
order by	t1.department_name

--soal 6
select		t1.employee_id,
			concat(t1.first_name,' ',t1.last_name) as fullname,
			t2.job_title,
			t1.salary - t1.tax as salary,
			t2.min_salary,
			t2.max_salary 
from 		employees t1
left join	jobs t2 on t1.job_id = t2.job_id
order by	t1.employee_id 
