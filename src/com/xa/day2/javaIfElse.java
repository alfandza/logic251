package com.xa.day2;

import java.util.Scanner;

public class javaIfElse {
    static int maxlulus = 100, lulus = 60, remed1 = 59, remed2 = 55;

    public static void main(String[] args) {
        //statementSatu();
        //statementDua();
        //statementEmpat();
    }
    // If else ketika diexecute,satu persatu, tidak disimpan memori sehingga lebih lama
    public static void statementSatu() {
        String nama, alamat;
        int age;
        Scanner scanner = new Scanner(System.in);

        System.out.print("Input nama : ");
        nama = scanner.nextLine();

        System.out.print("Input alamat : ");
        alamat = scanner.next();

        System.out.println("Input usia : ");
        age = scanner.nextInt();

        System.out.println("\n----------Cetak----------");
        System.out.println("Nama\t\t : " + nama);
        System.out.println("Alamat\t\t : " + alamat);
        System.out.println("Umur\t\t : " + age);
        scanner.close();
    }

    public static void statementDua() {
        if (28 > 18) {
            System.out.println("28 lebih besar dari 18");
        }

        int a = 28, b = 18;
        if (a > b) {
            System.out.println(a + " lebih besar dari " + b);
        } else if (18 < a) {
            System.out.println("18 lebih kecil dari " + a);
        } else {
            System.out.println("Tidak ketemu");
        }


    }

    public static void statementTiga() {
        int nilai;
        Scanner scanner = new Scanner(System.in);

        System.out.print("Input nilai : ");
        nilai = scanner.nextInt();

        if (nilai >= lulus && nilai <= maxlulus) {
            System.out.println("Selamat! Anda dinyatakan lulus! Nilai anda " + nilai);
        } else if (nilai >= remed2 && nilai <= remed1) {
            System.out.println("Mohon maaf, anda dinyatakan remidi! Nilai anda " + nilai);
        } else if (nilai < remed2) {
            System.out.println("Mohon maaf, anda dinyatakan gagal! Nilai anda " + nilai);
        } else {
            System.out.println("Mohon maaf, nilai yang anda masukkan salah!");
        }

        scanner.close();
    }

    public static void statementEmpat() {
        int nilai; String tipe="custom";
        Scanner scanner = new Scanner(System.in);

        System.out.print("Input nilai : ");
        nilai = scanner.nextInt();

        if (nilai >= lulus && nilai <= maxlulus) {
            System.out.println("Selamat! Anda dinyatakan lulus! Nilai anda " + nilai);
        } else if (nilai >= remed2 && nilai <= remed1) {
            if (tipe=="custom") {
                System.out.println("Selamat! Anda dinyatakan lulus!");
            } else {
                System.out.println("Mohon maaf, anda dinyatakan remidi! Nilai anda " + nilai);
            }
        } else if (nilai < remed2) {
            System.out.println("Mohon maaf, anda dinyatakan gagal! Nilai anda " + nilai);
        } else {
            System.out.println("Mohon maaf, nilai yang anda masukkan salah!");
        }

        scanner.close();
    }



}
