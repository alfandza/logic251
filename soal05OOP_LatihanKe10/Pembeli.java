package com.xa.extra.SoalHari10.Soal5OOP;

public class Pembeli {
    private String nama;
    private int uang;

    Pembeli(String nama, int uang){
        this.nama = nama;
        this. uang = uang;

    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public int getUang() {
        return uang;
    }

    public void setUang(int uang) {
        this.uang = uang;
    }

    public int beli (Laptop laptop, Mouse mouse){
        int hasil = this.uang - (laptop.getharga() + mouse.getharga());
        this.setUang(this.getUang()-(laptop.getharga() + mouse.getharga()));
        return hasil;
    }
    //public void beli(Laptop laptop)
}
