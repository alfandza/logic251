package com.xa.day10;

import java.util.Arrays;

public class javaSort {
    
    public static void main(String[] args) {
        String[] str = bubbleSortArray("3 5 1 4 2".split(" "));
        System.out.println(Arrays.toString(str)); // cetak array pake java util
        cetakArray(str); //cetak array pakai method
    }

    public static void cetakArray(Object[] str) {
        //cetak array manual menggunakan for
        for (Object kata : str) {
            System.out.print(kata + "\t");
        }
    }

    public static String[] bubbleSortArray(String[] stringArray){
        // (in) String : "3 5 1 4 2";
        // (out) int[] : {1, 2, 3, 4, 5}

        int swap = 0;
        // loop ke 1 untuk loop sebanyak item di array
        for (int i=0; i<stringArray.length;i++){
            //loop ke 2 untuk melakukan swap
            //j = 0  ascending
            //j = 1+i = descending
            for (int j=0; j < stringArray.length;j++){
                // System.out.println(stringArray[i]); // index 0
                // System.out.println(stringArray[j]); // index 1
                int a = Integer.parseInt(stringArray[i]);
                int b = Integer.parseInt(stringArray[j]);

                if (a<b){
                    swap = a;
                    stringArray[i] = stringArray[j];
                    stringArray[j] = String.valueOf(swap);
                }

            }

        }
        return stringArray;
    }



}
