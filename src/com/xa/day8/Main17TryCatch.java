package com.xa.day8;

public class Main17TryCatch {
    /* 
        try {
            //kode program run normal
        }
        catch (Exception e) {
            //ambil detail error - nambah text error berbahasa user
        }
    */

    public static void main(String[] args) {
        try {
            // ini akan terjadi error
            int a = 0;
            int b = 10;
            int c = a/b;
            System.out.println("Hasil "+a+" : "+b+" = "+c);
            // contohMultipleInstance();
        }
        catch (Exception e){

            if (e.getMessage().equals("/ by zero"))System.out.println("Pembagi tidak boleh 0");
            else System.out.println("Warning : " + e.getMessage());
        }
        finally {System.out.println("Tetap jalanwalaupun error");}
        System.out.println("Program selesai");
    }

    public static void contohMultipleInstance() {
        try {
            System.out.println("Code block dari level 1");
            int a = 40/0;
            System.out.println(a);

        }catch (Exception e){
            // System.out.println("--- catch pada block 1 + " e.getMessage());
            System.out.println("Error " + e.getMessage());
        }

        try {
            System.out.println("Code block dari level 1");
            int[] arry ={97,98,97,98};
            // index     0   1   2  3
            // length    1   2   3   4
            System.out.println(arry[5]);
        } catch (Exception e) {
            //TODO: handle exception
            System.out.println("Catch pada blok 2 : " + e.getMessage());
        }

    }
    
}
