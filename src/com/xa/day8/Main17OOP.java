package com.xa.day8;

public class Main17OOP {
    
    public static void main(String[] args) {
        Segitiga objSegitiga = new Segitiga();
        objSegitiga.alas = 50d;
        objSegitiga.tinggi = 10d;

        objSegitiga.hitungLuas();
        objSegitiga.hitungKeliling();
        objSegitiga.hitungRusuk();
        
        Segitiga objSegitiga1 = new Segitiga();
        objSegitiga1.alas = 5;
        objSegitiga1.tinggi = 10;

        //cara 1
        System.out.println("Luas Segitiga : " + objSegitiga1.hitungLuas1());
        
        //cara 2
        double keliling = objSegitiga1.hitungKeliling1();
        System.out.println("Keliling Segitiga : " + keliling);
        
        System.out.println("Rusuk Segitiga : " + objSegitiga1.hitungRusuk1());
        
    }    
}

class Segitiga {
    /* 
        attributes / properties / variable
        double alas = 10
        double tinggi = 10
        int rusuk = 3

        fungsi / method void
        hitung luas
        hitung keliling 
        hitung rusuk


        fungsi / method type data
        hitungluas double       (alas * tinggi)/2
        hitungkeliling double
        hitungrusuk int

    */

    //attribute / properties / variable
    double alas;
    double tinggi;
    int rusuk;
    double luas;
    double keliling;

    // method / function void
    void hitungLuas(){
        this.luas = (this.alas * this.tinggi) / 2;
        System.out.println("Luas segitiga " + this.luas);
    }

    void hitungKeliling(){
        double sisiMiring = (Math.sqrt(Math.pow(this.alas, 2)+Math.pow(this.tinggi,2)));
        this.keliling = this.alas + this.tinggi + sisiMiring;
        System.out.println("Keliling segitiga " + this.keliling);
    }

    void hitungRusuk(){
        this.rusuk = (1+1+1);
        System.out.println("Rusuk Segitiga : " + this.rusuk);
    }

    // method / function typedata
    double hitungLuas1(){
        this.luas = this.alas * this.tinggi* 0.5;
        return this.luas;
    }

    double hitungKeliling1(){
        double sisiMiring = (Math.sqrt(Math.pow(this.alas,2)+Math.pow(this.tinggi,2)));
        this.keliling = this.alas + this.tinggi + sisiMiring;
        return this.keliling;
    }

    double hitungRusuk1(){
        this.rusuk = (1+1+1);
        return this.rusuk;
    }

}