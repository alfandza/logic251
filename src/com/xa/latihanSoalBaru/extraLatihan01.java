//Soal TimeSoal

package com.xa.latihanSoalBaru;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;
import java.util.Scanner;

public class extraLatihan01 {
    static int hargaParkir = 3000;
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String replay = "Y";
        while (replay == "Y"){
            System.out.print("Masukkan soal : ");
            int soal = scan.nextInt();
            switch (soal){
                case 1:
                soalSatu(scan);
                break;
                case 2:
                soalDua(scan);
                break;
                case 3:
                soalTiga(scan);
                break;
                case 4:
                soalEmpat(scan);
                break;
                default:
                System.out.println("Soal not found");
            }
        }

        
    }

    public static void soalSatu(Scanner scan) {
        System.out.print("Input jam masuk (d/M/yyyy H.mm) = ");
        scan.nextLine();
        String input1 = scan.nextLine();
        System.out.print("Input jam keluar (d/M/yyyy H.mm) = ");
        String input2 = scan.nextLine();
        System.out.print("Pilih jam / menit : ");
        String input3 = scan.nextLine().toLowerCase();

        DateTimeFormatter format = DateTimeFormatter.ofPattern("d/M/yyyy H.mm");

        LocalDateTime jamMasuk = LocalDateTime.parse(input1, format);
        LocalDateTime jamKeluar = LocalDateTime.parse(input2, format);

        jamDif(jamMasuk, jamKeluar, input3);

    }

    public static void soalDua(Scanner scan) {
        int harga = hargaParkir;
        System.out.print("Masukkan harga yang diterima : ");
        int hargaAkh = scan.nextInt();
        int menit = hargaAkh / harga;
        int jam = menit / 60;
        menit = (menit%60);
        System.out.println(jam+" jam "+menit+" menit ");

    }

    public static void soalTiga(Scanner scan) {
        System.out.println("===SOAL KE 3===");
        System.out.print("Masukkan tanggal Pinjam : ");
        String tanggalPinjam = scan.nextLine();

        System.out.print("Pinjam berapa hari ? : ");
        int pinjamHari = scan.nextInt();

        System.out.print("Masukkan tanggal Pinjam : ");
        scan.nextLine();
        String tanggalKembali = scan.nextLine();

        DateTimeFormatter format = DateTimeFormatter.ofPattern("dd-MM-yyyy");

        LocalDate dateBefore = LocalDate.parse(tanggalPinjam,format);
        LocalDate dateNow = LocalDate.parse(tanggalKembali,format);
        int denda = 500;
        long d = ChronoUnit.DAYS.between(dateBefore,dateNow) - pinjamHari;
        System.out.println("Keterlambatan sejauh " +d+" hari");
        denda *= d;
        System.out.println("Denda yang harus dibayar adalah : " + denda);
        // System.out.println("Selisih hari dari " + dateBefore + " s/d " + locDate + " adalah : " + d);
    }

    public static void soalEmpat(Scanner scan) {
        System.out.println("===SOAL KE 4===Pembelian Balon===");
        System.out.println("Harga 1 balon 2000, beli 5 gratis 1");
        System.out.print("Mau beli dengan uang berapa ? ");
        int uangBeli = scan.nextInt();
        System.out.println("Balon yang didapat adalah "+beliBalon(uangBeli)+" balon");

    }

    //method

    public static int beliBalon(int uangBeli){
        int hargaJual = 2000;
        int count = uangBeli / hargaJual;
        if (count >= 5) count++;
        return count;
    }

    public static long jamDif(LocalDateTime jamIn, LocalDateTime jamOut, String permintaan){
        long beda = 0;
        if (permintaan.contains("jam")) beda = ChronoUnit.HOURS.between(jamIn, jamOut);
        else if (permintaan.contains("menit")) {
            beda = ChronoUnit.MINUTES.between(jamIn, jamOut);
            hargaParkir /= 60;
        }

        System.out.println("Bedanya adalah " + beda + " " +permintaan);
        long bayar = beda*hargaParkir;
        System.out.println("Bayarnya " + bayar);
        return beda;
    }        


    
}
