package com.xa.latihanLogic3;

import java.util.Scanner;

public class soalLogic03 {

    public static void main(String[] args) {

        System.out.println("\nSoal 1 kotak 1\n");
        int rows11 = 7, columns11 = 7, number11 = 0; //soal 1 kotak 1
        printKotakSatu1(rows11,columns11, number11);

        System.out.println("\nSoal 1 kotak 2\n");
        int rows12 = 7, columns12 = 7, number12 = 0; //soal 1 kotak 2
        printKotakSatu2(rows12,columns12, number12);

        System.out.println("\nSoal 1 kotak 3\n");
        int rows13 = 7, columns13 = 7, number13 = 0; //soal 1 kotak 3
        printKotakSatu3(rows13,columns13, number13);
    }

    public static void printKotakSatu1(int row, int column, int number){
        for (int i=0; i<row;i++) {
            for (int j=0; j<column; j++) {
                System.out.print(number + "\t");
                number++;
            }
            System.out.println();
            number-=column-1;
        }
    }

    public static void printKotakSatu2(int row, int column, int number) {
        for (int i=0; i<row;i++) {
            for (int j=0; j<column; j++) {
                if (i > 0 && i < row-1 && j > 0 && j < column-1) {
                    System.out.print(" \t");
                } else System.out.print(number + "\t");
                number++;
            }
            System.out.println();
            number-=column-1;
        }
    }

    public static void printKotakSatu3(int row, int column, int number){
        int sumNumber1 = 0;
        int sumNumberMiring = 0;
        String sumNumber2 = "";
        for (int i=0; i<row;i++) {
            for (int j=0; j<column; j++) {
                System.out.print(number + "\t");
                sumNumber1+=number;
                if (i == j) {
                    sumNumberMiring += number;
                }
                number++;
            }
            System.out.print(sumNumber1);
            sumNumber2 += sumNumber1 + "\t";
            sumNumber1-=sumNumber1;
            System.out.println();
            number-=column-1;
        }
        System.out.println(sumNumber2 + sumNumberMiring);
    }

}
