package com.xa.day2;

import java.util.Scanner;

public class javaSwitchLatihan {
    static int tanggal;
    static int minggu=10, senin=11, selasa=12, rabu=13, kamis=14, jumat=15, sabtu=16;

    public static void main(String[] args) {
        cekHari();
    }

    public static void cekHari() {
        System.out.print("Hari apa ini pada minggu ini? Masukkan tanggal : ");
        Scanner scanner = new Scanner(System.in);
        tanggal = scanner.nextInt();

        switch (tanggal) {
            case 10:
                System.out.println("Minggu");
                break;
            case 11:
                System.out.println("Senin");
                break;
            case 12:
                System.out.println("Selasa");
                break;
            case 13:
                System.out.println("Rabu");
                break;
            case 14:
                System.out.println("Kamis");
                break;
            case 15:
                System.out.println("Jumat");
                break;
            case 16:
                System.out.println("Sabtu");
                break;
            default:
                System.out.println("Out of range!");
        }
        scanner.close();
    }

}
