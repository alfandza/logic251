CREATE DATABASE db_hr
        WITH 
        OWNER = postgres
        ENCODING = 'UTF8'
        CONNECTION LIMIT = -1;

CREATE TABLE regions (
		region_id serial primary key,
		region_name character varying (30)
);

CREATE TABLE countries (
		country_id character (2) primary key,
		country_name character varying (40),
		region_id integer NOT NULL,
		FOREIGN KEY (region_id) REFERENCES regions (region_id) ON UPDATE CASCADE on DELETE CASCADE
);

CREATE TABLE locations (
		location_id SERIAL PRIMARY KEY,
		street_address character varying (40),
		postal_code character varying (12),
		city character varying (30) NOT NULL,
		state_province character varying (25),
		country_id CHARACTER (2) NOT NULL,
		FOREIGN KEY (country_id) REFERENCES countries (country_id) ON UPDATE cascade ON DELETE CASCADE
);

CREATE TABLE departments (
		department_id serial primary key,
		department_name character varying (30) NOT NULL,
		location_id INTEGER,
		FOREIGN KEY (location_id) REFERENCES locations (location_id) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE jobs (
		job_id SERIAL PRIMARY KEY,
		job_title CHARACTER VARYING (35) NOT NULL,
		min_salary NUMERIC (8, 2),
		max_salary NUMERIC (8, 2)
);

CREATE TABLE employees (
		employee_id SERIAL PRIMARY KEY,
		first_name CHARACTER VARYING (20),
		last_name CHARACTER VARYING (25) NOT NULL,
		email CHARACTER VARYING (100) NOT NULL,
		phone_number CHARACTER VARYING (20),
		hire_date DATE NOT NULL,
		job_id INTEGER NOT NULL,
		salary NUMERIC (8, 2) NOT NULL,
		manager_id INTEGER,
		department_id INTEGER,
        FOREIGN KEY (job_id) REFERENCES jobs (job_id) ON UPDATE CASCADE ON DELETE CASCADE,
		FOREIGN KEY (department_id) REFERENCES departments (department_id) ON UPDATE CASCADE ON DELETE CASCADE,
		FOREIGN KEY (manager_id) REFERENCES employees (employee_id) ON UPDATE CASCADE ON DELETE CASCADE
);


CREATE TABLE dependents (
		dependent_id SERIAL PRIMARY KEY,
		first_name CHARACTER VARYING (50) NOT NULL,
		last_name CHARACTER VARYING (50) NOT NULL,
		relationship CHARACTER VARYING (25) NOT NULL,
		employee_id INTEGER NOT NULL,
		FOREIGN KEY (employee_id) REFERENCES employees (employee_id) ON DELETE CASCADE ON UPDATE CASCADE
);

insert into regions (region_name) 
values 	('Eropa'),
		('Asia'),
		('Amerika Utara');

insert into countries (country_id, country_name, region_id)
values 	('RU','Rusia',1),
		('FR','Perancis',1),
		('DE','German',1),
		('ID','Indonesia',2),
		('MY','Malaysia',2),
		('SG','Singapura',2),
		('MX','Meksiko',3),
		('US','Amerika Serikat',3),
		('CA','Kanada',3);

insert into locations (street_address,postal_code,city,state_province,country_id)
values 	('Nikolskaya Street',109012,'Moscow','Moscow','RU'),
		('Neglinnaya Street',109045,'Moscow','Moscow','RU'),
		('Tulsa Street',4545,'Paris','Paris','FR'),
		('French Street',4545,'Paris','Paris','FR'),
		('German Street',68745,'Berlin','Berlin','DE'),
		('Berlin Street',68745,'Munich','Munich','DE'),
		('Jalan Gejayan',50642,'Sleman','DIY','ID'),
		('Jalan Gombel Lama',50264,'Semarang','Jawa Tengah','ID'),
		('Jalan Durian Runtuh',54687,'Kuala Lumpur','Kuala Lumpur','MY'),
		('Jalan Durian Jatuh',54687,'Kuala Lumpur','Kuala Lumpur','MY'),
		('Seletar Street',54487,'Seletar','Seletar','SG'),
		('Rochor Street',54487,'Rochor','Rochor','SG'),
		('Hermosilio Street',54487,'Hermosilio','Hermosilio','MX'),
		('Acapulco Street',54487,'Acapulco','Acapulco','MX'),
		('Mexico Street',54487,'Texas','Texas','US'),
		('Central Street',54487,'New York City','New York','US'),
		('Vancouver Street',54487,'Vancouver','Vancouver','CA'),
		('Quebec Street',54487,'Quebec','Quebec','CA');

insert into departments (department_name,location_id)
values 		('Information Technology',1),
			('Engineering Technology',2),
			('Social Engineering Technology',3),
			('Research',4),
			('Publc Relation',5),
			('Health Department',6),
			('Human Resource',7),
			('Recreation',8),
			('Department of Equipment',9),
			('Worker 1',10),
			('Worker 2',11),
			('Worker 3',12),
			('Worker 4',13),
			('Worker 5',14),
			('Worker 6',15),
			('Worker 7',16),
			('Worker 8',17),
			('Worker 9',18);

insert into jobs (job_title,min_salary,max_salary)
values 	('Director', 400000.00, 800000.00),
        ('Manager', 400000.00, 700000.00),
		('Supervisor', 400000.00, 600000.00),
		('Worker', 300000.00, 500000.00),
		('Cleaning Service', 200000.00, 400000.00);

insert into employees (first_name,last_name,email,phone_number,hire_date,job_id,salary,manager_id,department_id)
values 		('Alexei','Borodin','alexei@company.ru',5554321,'05/JUN/2005',1,800000.00,1,1),
			('Pavel','Morozov','pavel@company.ru',5554325,'05/JUN/2005',2,700000.00,2,1),
            ('Alexei','Bostov','alexB@company.ru',544321,'05/JUN/2006',1,800000.00,3,2),
			('Pavlik','Morozov','pavlik@company.ru',5444325,'05/JUN/2006',2,700000.00,4,2),
			('Monsieur','Enzo','enzo@company.fr',5554321,'05/JUL/2006',1,600000.00,5,3),
			('Lady','Aime','aime@company.fr',554625,'05/JUL/2006',2,600000.00,6,3),
			('Monsieur','Danse','danse@company.fr',4564321,'05/JUL/2006',1,600000.00,7,4),
			('Lady','Caroline','caroline@company.fr',21354325,'05/SEP/2006',2,600000.00,8,4),
			('Friedrich','Steiner','steiner@company.de',4564321,'05/SEP/2006',1,600000.00,9,5),
			('Boy','Hendrick','hendrick@company.de',21354325,'05/SEP/2006',1,600000.00,10,6),
			('Samsul','Samsudin','samsudin@company.id',21354325,'05/SEP/2006',1,500000.00,11,7),
			('Samsul','Samson','samsul@company.id',21354325,'05/SEP/2006',1,500000.00,12,8),
			('Mohd','Samson','samson@company.my',21354325,'05/JUN/2007',1,500000.00,13,9),
			('Mohd','Hendri','hendri@company.my',21354325,'05/JUN/2007',1,500000.00,14,10),
			('Ling','Hendri','ling@company.sg',21354325,'05/JUN/2007',1,500000.00,15,11),
			('Ling','Lung','lingLung@company.sg',21354325,'05/MAY/2008',1,500000.00,16,12),
			('Cesar','Hermano','cesar@company.mx',21354325,'05/MAY/2008',1,500000.00,17,13),
			('Hermano','Hendry','hermano@company.mx',21354325,'05/MAY/2008',1,500000.00,18,14),
			('John','Doe','john@company.com',21354325,'05/MAY/2008',1,500000.00,19,15),
			('Doe','John','Doe@company.com',21354325,'05/MAY/2008',1,500000.00,20,16),
			('Handy','Hand','handy@company.ca',21354325,'05/MAY/2008',1,500000.00,21,17),
			('John','Smith','smith@company.ca',21354325,'05/MAY/2010',1,500000.00,22,18);

insert into dependents (first_name, last_name, relationship, employee_id)
select		first_name first_name,
			first_name last_name,
			'Married' relationship,
			employee_id employee_id
from 		employees;

-- For Practice 1 Menunjukkan Masa Kerja

select 	first_name, 
		last_name, 
		hire_date,
		age(now(), hire_date) as masa_kerja
from 	employees;

-- For Practice 2 menunjukkan total pekerja di negara

select		country.country_name,
			dep.department_id,
			dep.department_name,
            count(employ.employee_id)
from 		employees employ
inner join	departments dep
on employ.department_id = dep.department_id
inner join	locations loc
on dep.location_id = loc.location_id
inner join countries country
on loc.country_id = country.country_id
group by dep.department_id, loc.location_id, country.country_id
order by country.country_id, dep.department_id, loc.location_id;