package com.xa.day3;

import java.util.*;

public class javaArray {
    /* 
    *   Array : ada alokasi ukuran saat pembuatan, gak bisa berubah ukuran
                nggak bisa ditambah

        ArrayList : tidak ada alokasi ukuran, bisa berubah ukuran, bisa ditambah
    */
    public static void main(String[] args) {
        // // arrayInt01();
        // // arrayDuaDimensi();

        // //deklarasi array
        // ArrayList<String> cars = new ArrayList<String>();
        
        // // nambah data
        // cars.add("Nissan");
        // cars.add("Toyota");

        // // cetak data
        // System.out.println(cars);
        
        // // update data
        // cars.set(0, "Daihatsu");
        // System.out.println(cars);

        // // hapus data
        // cars.remove(1);
        // System.out.println(cars);
        soalSplit();
    }

    public static void arrayInt01() {
        // int Array
        // deklarasi array int - contoh 1
        int[] names;
        names = new int[3]; // alokasi memori dgn lengthnya

        // deklarasi array int - contoh 2
        int[] cars = new int[5];

        // deklarasi array int - contoh 3
        int[] nilaiArray = {90, 80, 70};
        // index        0   1   2
        // length       1   2   3

        // akses nilai item pada array
        System.out.println("Akses nilai item pada array nilai, index 0 : " + nilaiArray[0]);
        System.out.println("Akses nilai item pada array nilai, index 1 : " + nilaiArray[1]);
        System.out.println("Akses nilai item pada array nilai, index 2 : " + nilaiArray[2]);
        
        //mengambil jumlah item yang ada di array
        System.out.println("Isi dari array nilai adalah : " + nilaiArray.length);

        //mengubah nilai item pada array
        nilaiArray[1] = 100;
        System.out.println("Mengubah array dari 80 menjadi " + nilaiArray[1]);

        System.out.println("Aksses array pakai for");
        for (int i = 0; i<nilaiArray.length; i++) {
            System.out.println("Nilai item pada index " + i + " adalah : " + nilaiArray[i]);
        }
        
        System.out.println("Aksses array pakai for II");
        for (int i : nilaiArray) System.out.println(i);

    }


    public static void arrayDuaDimensi() {
                /* 
            array 2 dimensi
                        index      0  1   2 col
            array[] array2D =  0 {{90,80,70},  
                           r   1  {95,65},     
                           o   2  {75,50}};    
                           w   array2D[0][0] = 90
                               array2D[1][1] = 65
                               array2D[2][1] = 50
        
        */
        int [][] array2D = {{90,80,70}, {95,65}};

        //cetak nilai array [index row] [index column]
        System.out.println("Cetak row 0 col 0 : " + array2D[0][0]);
        System.out.println("Cetak row 1 col 1 : " + array2D[1][1]);
        System.out.println("Cetak row 0 col 2 : " + array2D[0][2]);
    }

    public static void soalSplit(){
        String input = "Aku Sayang Kamu";

        // 1. split string jadi string array
        // (pakai fungsi yang ada di java)
        String[] strArry = input.split(" ");

        // 2. looping strArry
        for (int i=0; i< strArry.length; i++) {
            //3. looping item strArry
            String newKata = "";
            int panjangKata = strArry[i].length();
            for (int j=0; j<panjangKata; j++) {
                char temp = strArry[i].charAt(j);
                if (j != 0 && j != panjangKata-1){
                    newKata += "*";
                }
                else{
                    newKata += Character.toString(temp);
                }
                
            }
            System.out.print(newKata + " ");
        }

    }

}
