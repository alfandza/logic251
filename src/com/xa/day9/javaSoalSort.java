package com.xa.day9;

// import java.util.Arrays;
import java.util.Scanner;

public class javaSoalSort {
    
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        problemOne(scan);
        problemTwo(scan);
        problemThree(scan);    
    }

    public static void problemOne(Scanner scan) {
        // input 9,2,5,3,4,1,2
        // output 1,2,2,3,4,5,9
        System.out.println("=====SOAL PERTAMA====");
        System.out.print("Masukkan deret angka : ");
        String[] deret = scan.nextLine().split(",");

        deret = bubbleSortArray(deret);
        printArry(deret);
        // System.out.println("Keluaran Program : " + Arrays.toString(deret));
    }

    public static void problemTwo(Scanner scan) {
        System.out.println("=====SOAL KEDUA====");
        System.out.print("Masukkan input : ");
        String kalimat = scan.nextLine().toLowerCase();
        char[] urutHuruf = kalimat.toCharArray();
        urutHuruf = characterArray(urutHuruf);
        printArryChar(urutHuruf);
        // // Cara ke 1 - menggunakan to char array
        // char[] charArray = kalimat.toCharArray();
        // Arrays.sort(charArray);
        // System.out.println("Output cara ke 1 : " + Arrays.toString(charArray));
        
       
    }

    public static void problemThree(Scanner scan) {
        System.out.println("====SOAL KETIGA====");
        System.out.print("Masukkan deret spasi : ");
        String[] deretSpasi = scan.nextLine().split(" ");
        deretSpasi = bubbleSortArray(deretSpasi);
        printArry(deretSpasi);
        findMed(deretSpasi);

    }

    //Method

    public static void findMed(String[] string){
        int indexMedian = string.length/2;
        int median = Integer.parseInt(string[indexMedian]);

        System.out.println("Nilai Median : " + median);
        System.out.println("Pada index ["+indexMedian+"]");
    }

    public static void printArry(Object[] namaArray) {
        for (int i=0; i<namaArray.length; i++){
            Object temp = namaArray[i];
            System.out.print(temp);
        }
        System.out.println();
    }

    public static void printArryChar(char[] namaArray) {
        for (int i=0; i<namaArray.length; i++){
            Object temp = namaArray[i];
            System.out.print(temp);
        }
        System.out.println();
    }

    public static String[] bubbleSortArray(String[] stringArray){
        // (in) String : "3 5 1 4 2";
        // (out) int[] : {1, 2, 3, 4, 5}

        int swap = 0;
        // loop ke 1 untuk loop sebanyak item di array
        for (int i=0; i<stringArray.length;i++){
            //loop ke 2 untuk melakukan swap
            //j = 0  ascending
            //j = 1+i = descending
            for (int j=0; j < stringArray.length;j++){
                // System.out.println(stringArray[i]); // index 0
                // System.out.println(stringArray[j]); // index 1
                int a = Integer.parseInt(stringArray[i]);
                int b = Integer.parseInt(stringArray[j]);

                if (a<b){
                    swap = a;
                    stringArray[i] = stringArray[j];
                    stringArray[j] = String.valueOf(swap);
                }

            }

        }
        return stringArray;
    }

    public static char[] characterArray(char[] urutHuruf) {
        char swap = 0;
        // loop ke 1 untuk loop sebanyak item di array
        for (int i=0; i<urutHuruf.length;i++){
            //loop ke 2 untuk melakukan swap
            //j = 0  ascending
            //j = 1+i = descending
            for (int j=0; j < urutHuruf.length;j++){
                char a = urutHuruf[i];
                char b = urutHuruf[j];
                if (a<b){
                    swap = a;
                    urutHuruf[i] = urutHuruf[j];
                    urutHuruf[j] = swap;
                }
            }
        }
        return urutHuruf;
    }

    
    public static String[] spasiSortArray(String[] stringArrSpace) {
      // (in) String : "3 5 1 4 2";
        // (out) int[] : {1, 2, 3, 4, 5}

        int swap = 0;
        // loop ke 1 untuk loop sebanyak item di array
        for (int i=0; i< stringArrSpace.length; i++) {
            // loop ke 2 untuk melakukan swap
            // j = 0 ascending
            // j = 1+i = descending
            for (int j = 0; j < stringArrSpace.length; j++) {
                // System.out.println(stringArray[i]); // index 0
                // System.out.println(stringArray[j]); // index 1
                int a = Integer.parseInt(stringArrSpace[i]);
                int b = Integer.parseInt(stringArrSpace[j]);

                if (a < b) {
                    swap = a;
                    stringArrSpace[i] = stringArrSpace[j];
                    stringArrSpace[j] = String.valueOf(swap);
                }

            }

        }
        return stringArrSpace;
    }

}
    
