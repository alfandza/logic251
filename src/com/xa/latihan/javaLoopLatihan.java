package com.xa.latihan;

public class javaLoopLatihan {
    static int N;
    public static void main(String[] args) {
        soalSatuSampaiTiga();
        soalEmpatSampaiLima();
        soalEnamSampaiTujuh();
        soalDelapansampaiSembilan();
        soalSepuluh();
    }

    public static void soalSatuSampaiTiga(){
        int a=1,b=2,c=1;
        System.out.println("Soal 01");
        // 1 3 5 7 9 13
        for (N=1; N<=7; N++) {
            System.out.print(a+" ");
            a+=2;
        }

        System.out.println("\n\nSoal 02");
        // 2 4 6 8 10 12 14
        for (N=1; N<=7; N++) {
            System.out.print(b+" ");
            b+=2;
        }

        System.out.println("\n\nSoal 03");
        // 1 4 7 10 13 16 19
        for (N=1; N<=7; N++) {
            System.out.print(c+" ");
            c+=3;
        }
    }

    public static void soalEmpatSampaiLima() {
        int d=1,e=1;
        System.out.println("\n\nSoal 04");
        // 1 5 . 9 13 . 17
        for (N=1; N<=7; N++) {
            System.out.print(d+" ");
            d+=4;
            if (d == 9 || d == 17) {
                System.out.print(". ");
            }
            else if (d > 17) {
                break;
            }
        }

        System.out.println("\n\nSoal 05");
        // 1 5 . 13 17 . 25
        for (N=1; N<=7; N++) {
            if (e == 9 || e == 21) {
                System.out.print(". ");
                e+=4;
                continue;
            }
            System.out.print(e+" ");
            e+=4;
        }
    }

    public static void soalEnamSampaiTujuh() {
        int f=2,g=3;
        System.out.println("\n\nSoal 06");
        // 2 4 8 16 32 64 128
        for (N=1; N<=7; N++) {
            System.out.print(f+" ");
            f*=2;
        }

        System.out.println("\n\nSoal 07");
        // 3 9 27 81 243 729 8127
        for (N=1; N<=7; N++) {
            System.out.print(g+" ");
            g*=3;
        }
    }

    public static void soalDelapansampaiSembilan() {
        int h=4,i=3;
        System.out.println("\n\nSoal 08");
        // 4 16 . 64 256 . 1024
        // bisa juga pakai N, setiap N = 3 gunakan if (N % 3 == 0) cetak .
        for (N=1; N<=7; N++) {
            System.out.print(h+" ");
            h*=4;
            if (h == 64) {
                System.out.print(". ");
            }else if (h == 1024) {
                System.out.print(". ");
            }else if (h > 1024) {
                break;
            }
        }

        System.out.println("\n\nSoal 09");
        // 3 9 27 xxx 243 729 2187
        for (N=1; N<=7; N++){
            if (i == 81) {
                System.out.print("xxx ");
                i*=3;
                continue;
            }
            System.out.print(i + " ");
            i*=3;
        }
    }

    public static void soalSepuluh() {
        int j=2;
        System.out.println("\n\nSoal 10");
        // 2 3 5 7 11 13 17
        // will be fixed later
        for (N=1; N<=7; N++) {
            System.out.print(j + " ");
            if (j == 2) {
                j+=1;
                continue;
            }else if (j == 13) {
                j+=4;
                System.out.print(j + " ");
                continue;
            }
            j+=2;
        }

    }

}
