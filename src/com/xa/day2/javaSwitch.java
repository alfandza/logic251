package com.xa.day2;

public class javaSwitch {

    public static void main(String[] args) {
        switchSatu();
    }

    public static void switchSatu() {
        String tipe = "Custom";

        /*  Switch (membandingkan kondisi sedikit, tidak bisa pakai comparision,
        *           langsung nyimpen ke memori sehingga lebih cepat)
        *   switch(expression) {
        *       case x:
        *           //code block x
        *       break;
        *       case y:
        *           //code block y
        *       break;
        *       default:
        *           //code block default
        *       } */

        switch (tipe) {
            case "Reguler":
                System.out.println("Kategori : " +tipe);
            break;
            case "Custom":
                System.out.println("Kategori : " +tipe);
            break;
            default:
                System.out.println("Kategori : Paylater");
        }

    }

}
