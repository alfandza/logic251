-- Database: db_mahasiswa

-- DROP DATABASE db_mahasiswa;

CREATE DATABASE db_mahasiswa
    WITH 
    OWNER = postgres
    ENCODING = 'UTF8'
    LC_COLLATE = 'en_US.UTF-8'
    LC_CTYPE = 'en_US.UTF-8'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1;

-- Table: public.tbl_m_agama

-- DROP TABLE public.tbl_m_agama;

CREATE TABLE public.tbl_m_agama
(
    id_agama_pk integer NOT NULL DEFAULT nextval('tbl_m_agama_id_agama_pk_seq'::regclass),
    kode_agama character(5) COLLATE pg_catalog."default" NOT NULL,
    deskripsi character varying(20) COLLATE pg_catalog."default" NOT NULL,
    is_active bit(1),
    created_by character varying(50) COLLATE pg_catalog."default",
    created_date date NOT NULL,
    updated_by character varying(50) COLLATE pg_catalog."default",
    updated_date date
)

TABLESPACE pg_default;

ALTER TABLE public.tbl_m_agama
    OWNER to postgres;

CREATE TABLE tbl_m_type_dosen (
		id_type_dosen_pk serial primary key,
		kode_type_dosen character(5) not null,
		deskripsi character varying(20) not null,
		is_active bit,
		created_by character varying(50) not null,
		created_date date not null,
		updated_by character varying(50) not null,
		updated_date date not null
);

create table tbl_m_jurusan (
		id_jurusan_pk serial primary key,
		kode_jurusan character(5) not null,
		nama_jurusan character varying(50) not null,
		is_active bit,
		created_by character varying(50) not null,
		created_date date not null,
		updated_by character varying(50) not null,
		updated_date date not null
);

create table tbl_m_dosen (
		id_dosen_pk bigserial primary key,
		kode_dosen character(5) not null,
		nama_dosen character varying(100) not null,
		id_jurusan_fk int not null,
		id_type_dosen_fk int not null,
		is_active bit,
		created_by character varying(50) not null,
		created_date date not null,
		updated_by character varying(50) not null,
		updated_date date not null
);

create table tbl_m_mahasiswa (
		id_mahasiswa_pk bigserial primary key,
		kode_mahasiswa character(5) not null,
		nama_dosen character varying(100) not null,
		alamat character varying(200) not null,
		id_jurusan_fk int not null,
		id_agama_fk int not null,
		is_active bit,
		created_by character varying(50) not null,
		created_date date not null,
		updated_by character varying(50) not null,
		updated_date date not null
);

create table tbl_r_ujian (
		id_ujian_pk serial primary key,
		kode_ujian character(5) not null,
		nama_ujian character varying(50) not null,
		is_active bit,
		created_by character varying(50) not null,
		created_date date not null,
		updated_by character varying(50) not null,
		updated_date date not null
);

create table tbl_t_nilai (
		id_nilai_pk serial primary key,
		id_mahasiswa_pk bigint not null,
		id_ujian_fk int not null,
		nilai NUMERIC (8, 2) not null,
		is_active bit,
		created_by character varying(50) not null,
		created_date date not null,
		updated_by character varying(50) not null,
		updated_date date not null
);

insert into tbl_m_agama (kode_agama,deskripsi, created_by,created_date,updated_by,updated_date,is_active)
values 	('A001','Islam','adit','2019-10-30 13:48:49.873','adit','2019-10-30 13:48:49.873','1'),
		('A002','Kristen','adit','2019-10-30 13:48:49.873','adit','2019-10-30 13:48:49.873','1'),
		('A003','Katolik','adit','2019-10-30 13:48:49.873','adit','2019-10-30 13:48:49.873','1'),
		('A004','Hindu','adit','2019-10-30 13:48:49.873','adit','2019-10-30 13:48:49.873','1'),
		('A005','Buddha','adit','2019-10-30 13:48:49.873','adit','2019-10-30 13:48:49.873','1');

insert into tbl_m_type_dosen (kode_type_dosen,deskripsi,created_by,created_date,updated_by,updated_date,is_active)
values 	('T001','Tetap','adit','2019-10-30 13:48:49.873','adit','2019-10-30 13:48:49.873','1'),
		('T002','Honorer','adit','2019-10-30 13:48:49.873','adit','2019-10-30 13:48:49.873','1'),
		('T003','Expertise','adit','2019-10-30 13:48:49.873','adit','2019-10-30 13:48:49.873','1');

insert into tbl_m_jurusan (kode_jurusan,nama_jurusan, created_by,created_date,updated_by,updated_date,is_active)
values 	('J001','Teknik Informatika','adit','2019-10-30 13:48:49.873','adit','2019-10-30 13:48:49.873','1'),
		('J002','Management Informatika','adit','2019-10-30 13:48:49.873','adit','2019-10-30 13:48:49.873','1'),
		('J003','Sistem Informasi','adit','2019-10-30 13:48:49.873','adit','2019-10-30 13:48:49.873','1'),
		('J004','Sistem Komputer','adit','2019-10-30 13:48:49.873','adit','2019-10-30 13:48:49.873','1'),
		('J005','Komputer Akuntansi','adit','2019-10-30 13:48:49.873','adit','2019-10-30 13:48:49.873','1');

insert into tbl_m_dosen (kode_dosen,nama_dosen, id_jurusan_fk , id_type_dosen_fk, created_by,created_date,updated_by,updated_date,is_active)
values 	('D001','Prof. Dr. Retno Wahyuningsih',1,2,'adit','2019-10-30 13:48:49.873','adit','2019-10-30 13:48:49.873','1'),
		('D002','Prof. Roy M. Sutikno',2,1,'adit','2019-10-30 13:48:49.873','adit','2019-10-30 13:48:49.873','1'),
		('D003','Prof. Hendri A. Verburgh',3,2,'adit','2019-10-30 13:48:49.873','adit','2019-10-30 13:48:49.873','1'),
		('D004','Prof. Risma Suparwata',4,2,'adit','2019-10-30 13:48:49.873','adit','2019-10-30 13:48:49.873','1'),
		('D005','Prof. Amir Sjarifuddin Madjid, MM, MA',5,2,'adit','2019-10-30 13:48:49.873','adit','2019-10-30 13:48:49.873','1');

insert into tbl_m_mahasiswa (kode_mahasiswa,nama_mahasiswa, alamat, id_agama_fk,id_jurusan_fk, created_by,created_date,updated_by,updated_date,is_active)
values 	('M001','Budi Gunawan','Jl. Mawar No 3 RT 05 Cicalengka, Bandung',1,1,'adit','2019-10-30 13:48:49.873','adit','2019-10-30 13:48:49.873','1'),
		('M002','Rinto Raharjo','Jl. Kebagusan No. 33 RT04 RW06 Bandung',1,2,'adit','2019-10-30 13:48:49.873','adit','2019-10-30 13:48:49.873','1'),
		('M003','Asep Saepudin','Jl. Sumatera No. 12 RT02 RW01, Ciamis',1,3,'adit','2019-10-30 13:48:49.873','adit','2019-10-30 13:48:49.873','1'),
		('M004','M. Hafif Isbullah','Jl. Jawa No 01 RT01 RW01, Jakarta Pusat',2,1,'adit','2019-10-30 13:48:49.873','adit','2019-10-30 13:48:49.873','1'),
		('M005','Cahyono','Jl. Niagara No. 54 RT01 RW09, Surabaya',3,2,'adit','2019-10-30 13:48:49.873','adit','2019-10-30 13:48:49.873','1');

insert into public.tbl_r_ujian (kode_ujian,nama_ujian,created_by,created_date,updated_by,updated_date,is_active)
values 	('U001','Algoritma','adit','2019-10-30 13:48:49.873','adit','2019-10-30 13:48:49.873','1'),
		('U002','Aljabar','adit','2019-10-30 13:48:49.873','adit','2019-10-30 13:48:49.873','1'),
		('U003','Statistika','adit','2019-10-30 13:48:49.873','adit','2019-10-30 13:48:49.873','1'),
		('U004','Etika Profesi','adit','2019-10-30 13:48:49.873','adit','2019-10-30 13:48:49.873','1'),
		('U005','Bahasa Inggris','adit','2019-10-30 13:48:49.873','adit','2019-10-30 13:48:49.873','1');

insert into tbl_t_nilai (id_nilai_pk,id_mahasiswa_pk,id_ujian_fk,nilai,created_by,created_date,updated_by,updated_date,is_active)
values	(1,4,1,90,'adit','2019-10-30 13:48:49.873','adit','2019-10-30 13:48:49.873','1'),
		(2,1,1,80,'adit','2019-10-30 13:48:49.873','adit','2019-10-30 13:48:49.873','1'),
		(3,2,3,85,'adit','2019-10-30 13:48:49.873','adit','2019-10-30 13:48:49.873','1'),
		(4,4,2,95,'adit','2019-10-30 13:48:49.873','adit','2019-10-30 13:48:49.873','1'),
		(5,5,5,70,'adit','2019-10-30 13:48:49.873','adit','2019-10-30 13:48:49.873','1');


--SOAL NO 2 mengubah tipe data
alter table tbl_m_dosen alter column nama_dosen type character varying(200);

--SOAL NO 3 tampilkan data mahasiswa join
select 		mahasiswa.kode_mahasiswa,
			mahasiswa.nama_mahasiswa,
			jurusan.nama_jurusan,
			case 	when agama.deskripsi is null then '-'
					else agama.deskripsi
					END
			as agama
from 		tbl_m_mahasiswa mahasiswa

left join tbl_m_jurusan jurusan
on mahasiswa.id_jurusan_fk = jurusan.id_jurusan_pk

left join tbl_m_agama agama
on mahasiswa.id_agama_fk = agama.id_agama_pk

order by mahasiswa.kode_mahasiswa

/* 
inner join dan join hanya untuk yang secircle
left join akan mulai dari mahasiswa
right join akan mulai dari agama

kalau ada case, gunakan
		CASE 	WHEN conditionA THEN resultA
				ELSE result
				END
wajib when dan else


*/

--SOAL NO 4
select 		mahasiswa.kode_mahasiswa,
			mahasiswa.nama_mahasiswa,
			jurusan.nama_jurusan,
			case 	when jurusan.is_active = '1' then 'AKTIF'
					else 'NON-AKTIF'
			end
from 		tbl_m_mahasiswa mahasiswa

left join tbl_m_jurusan jurusan
on mahasiswa.id_jurusan_fk = jurusan.id_jurusan_pk

where jurusan.is_active = '0'
order by mahasiswa.kode_mahasiswa

--SOAL NO 5
select 		mahasiswa.kode_mahasiswa,
			mahasiswa.nama_mahasiswa,
			nilai.nilai,
			ujian.nama_ujian,
			case 	when ujian.is_active = '1' then 'AKTIF'
					else 'NON AKTIF'
			end as status_ujian
from 		tbl_m_mahasiswa mahasiswa
inner join tbl_t_nilai nilai on mahasiswa.id_mahasiswa_pk = nilai.id_mahasiswa_pk
								and nilai.nilai > 80
inner join tbl_r_ujian ujian on nilai.id_ujian_fk = ujian.id_ujian_pk 
where ujian.is_active = '1';

--SOAL NO 6

select * from tbl_m_jurusan 
where nama_jurusan like '%Sistem%' 
order by id_jurusan_pk asc;

--SOAL NO 7
select  	m.nama_mahasiswa,
			count(n.id_ujian_fk) as total_ujian
from 		tbl_t_nilai n
inner join tbl_m_mahasiswa m on m.id_mahasiswa_pk = n.id_mahasiswa_pk
group by m.nama_mahasiswa

--SOAL NO 8
select 		mahasiswa.kode_mahasiswa,
			mahasiswa.nama_mahasiswa,
			jurusan.nama_jurusan,
			agama.deskripsi,
			dosen.nama_dosen,
			case 	when jurusan.is_active = '1' then 'AKTIF'
					else 'NON-AKTIF'
			end as status_jurusan,
			type_dos.deskripsi
from 		tbl_m_mahasiswa mahasiswa
left join tbl_m_jurusan jurusan
on mahasiswa.id_jurusan_fk = jurusan.id_jurusan_pk

left join tbl_m_agama agama 
on mahasiswa.id_agama_fk = agama.id_agama_pk 

left join tbl_m_dosen dosen 
on jurusan.id_jurusan_pk = dosen.id_jurusan_fk 

left join tbl_m_type_dosen type_dos
on dosen.id_type_dosen_fk = type_dos.id_type_dosen_pk 

order by mahasiswa.kode_mahasiswa

--SOAL NO 9
create view mahasiswa_ke_dosen As
select 		mahasiswa.kode_mahasiswa,
			mahasiswa.nama_mahasiswa,
			jurusan.nama_jurusan,
			agama.deskripsi as agama,
			dosen.nama_dosen,
			case 	when jurusan.is_active = '1' then 'AKTIF'
					else 'NON-AKTIF'
			end as status_jurusan,
			type_dos.deskripsi as status
from 		tbl_m_mahasiswa mahasiswa
left join tbl_m_jurusan jurusan
on mahasiswa.id_jurusan_fk = jurusan.id_jurusan_pk

left join tbl_m_agama agama 
on mahasiswa.id_agama_fk = agama.id_agama_pk 

left join tbl_m_dosen dosen 
on jurusan.id_jurusan_pk = dosen.id_jurusan_fk 

left join tbl_m_type_dosen type_dos
on dosen.id_type_dosen_fk = type_dos.id_type_dosen_pk 

order by mahasiswa.kode_mahasiswa

select * from mahasiswa_ke_dosen


--SOAL NO 10
select 		mahasiswa.kode_mahasiswa,
			mahasiswa.nama_mahasiswa,
			nilai.nilai,
			ujian.nama_ujian,
			case 	when ujian.is_active = '1' then 'AKTIF'
					else 'NON AKTIF'
			end as status_ujian
from 		tbl_m_mahasiswa mahasiswa
left join tbl_t_nilai nilai on mahasiswa.id_mahasiswa_pk = nilai.id_mahasiswa_pk
left join tbl_r_ujian ujian on nilai.id_ujian_fk = ujian.id_ujian_pk 

