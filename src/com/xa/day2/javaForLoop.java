package com.xa.day2;

public class javaForLoop {

    public static void main(String[] args) {
        forLoopSatu();
    }

    public static void forLoopSatu() {
        /*
        *   for (initialization;
        *               condition;
        *               statement) { //optional
        *             // code block
        *       }
        *  init -> condition -> code block -> statement
        *  Untuk penggunaan lebih ribet, semua bahasa ada, lebih sering digunakan
        * */

//        for (int i=0; i<5; i++) {
//            System.out.println(i+1);
//        }

        int count=10;
        for (int i=1; i<=5; i++) {
            System.out.println(count);
            count--;
        }

    }

}
