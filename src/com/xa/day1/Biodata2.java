package com.xa.day1;

public class Biodata2 {
    private long nik;
    private String nama;
    private String tempat;
    private String tanggal;
    private String kelamin;
    private String alamat;

    Biodata2(long nik, String nama, String tempat, String tanggal, String kelamin, String alamat) {
        this.setNik(nik);
        this.setNama(nama);
        this.setTempat(tempat);
        this.setTanggal(tanggal);
        this.setKelamin(kelamin);
        this.setAlamat(alamat);
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getKelamin() {
        return kelamin;
    }

    public void setKelamin(String kelamin) {
        this.kelamin = kelamin;
    }

    public String getTanggal() {
        return tanggal;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }

    public String getTempat() {
        return tempat;
    }

    public void setTempat(String tempat) {
        this.tempat = tempat;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public long getNik() {
        return nik;
    }

    public void setNik(long nik) {
        this.nik = nik;
    }
}
