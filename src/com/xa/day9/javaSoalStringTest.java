package com.xa.day9;

import java.util.Scanner;

public class javaSoalStringTest {
    
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        soalSembilan(scan);
        // soalSepuluh(scan);
    }

    public static void soalSepuluh(Scanner scan) {
        
        System.out.print("Masukkan inputan 1 : ");
        String check = scan.nextLine();
        System.out.print("Masukkan inputan 2 : ");
        String input = scan.nextLine();
        int count = 0;
        // int checkLength,inputLength;


        for (int i=0;i<input.length();i++){
            char temp = input.charAt(i);
            for (int j=0;j<check.length();j++){
                char checkChar = check.charAt(j);
                if (checkChar == temp) count++;
            }

        }


        System.out.println("Input 1 : " + check);
        System.out.println("Input 2 : " + input);
        if (count>0) System.out.println("YES! " + count);
        else System.out.println("NO");
    }


    public static void soalSembilan(Scanner scan) {
        System.out.println("Hapus berapa karakter agar anagram");
        System.out.print("Masukkan input pertama : ");
        // scan.nextLine();
        String inputA = scan.nextLine();
        System.out.print("Masukkan input kedua : ");
        String inputB = scan.nextLine();
        System.out.println("Input");
        System.out.println(inputA);
        System.out.println(inputB);


        for (int i=0;i<inputA.length();i++){
            char temp1 = inputA.charAt(i);
            for (int j=0;j<inputB.length();j++){
                char temp2 = inputB.charAt(j);
                String change = Character.toString(temp1);
                inputA = inputA.replace(change, "");
                inputB = inputB.replace(change, "");
                i=-1;
                j=0;
            }
        }

        System.out.println(inputA + " " + inputB);
        System.out.println(inputA.length()+inputB.length());
    }
}
