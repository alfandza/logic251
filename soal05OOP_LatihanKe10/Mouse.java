package com.xa.extra.SoalHari10.Soal5OOP;

public class Mouse {
    private String nama;
    private int harga;

    Mouse(String nama, int harga){
        this.nama = nama;
        this.harga = harga;
    }


    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public int getharga() {
        return harga;
    }

    public void setharga(int harga) {
        this.harga = harga;
    }
}
