package com.xa.day11;

import java.util.*;

public class javaSoalSimulasi {
    
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String replay = "Y";
        while (replay.equals("Y")){
            System.out.print("Masukkan soal : ");
            int soal = scan.nextInt();
            switch (soal){
                case 1:
                    numberOne(scan);
                    break;
                case 2:
                    numberTwo(scan);
                    break;
                case 5:
                    numberFive(scan);
                    break;
                case 6:
                    numberSix(scan);
                    break;
                case 7:
                    numberSeven(scan);
                    break;
                case 8:
                    numberEight(scan);
                    break;
                default:
                    System.out.println("Salah nomer!");
            
            }
            System.out.print("Mau lagi ? (Y/N) ");
            scan.nextLine();
            replay = scan.nextLine().toUpperCase();
        }
        
    }

    //Soal
    public static void numberOne(Scanner scan) {
        System.out.println("====SOAL SIMULASI 01====\nMasukkan inputan dari 0-1000");
        System.out.print("Masukkan input pertama : ");
        int inputPertama = scan.nextInt();
        System.out.print("Masukkan input kedua : ");
        int inputKedua = scan.nextInt();
        int random = randomNum();
        System.out.println(checkNum(inputPertama, inputKedua, random));
    }

    public static void numberTwo(Scanner scan) {
        System.out.println("Password Checker");
        System.out.print("Masukkan input : ");
        scan.nextLine();
        String input = scan.nextLine();
        String specialChar = "!@#S%^&*()-+";
        int level = 0;
        System.out.println("Soal Nomer 2");
        System.out.println("Input Password : " + input);
        
        //Char array for specialChar
        char[] specialCharArry = new char[specialChar.length()];
        for(int i=0;i<specialChar.length();i++) {
            specialCharArry[i] = specialChar.charAt(i);
        }

        //check if password at least 6

        if (input.length() > 5) level++;

        //Check Special Character
        for (int i=0;i<input.length();i++){
            char temp = input.charAt(i);
            for (int j=0; j<specialCharArry.length;j++) {
                char check = specialCharArry[j];
                if (temp == check) {
                    level++;
                    break;
                }
            }
        }

        //Check angka 
        for (int i=0;i<input.length();i++){
            char temp = input.charAt(i);
            if (temp >= 48 && temp <=57) {
                level++;
                break;
            }
        }
        
        //Check lowercase
        for (int i=0;i<input.length();i++){
            char temp = input.charAt(i);
            if (temp >= 97 && temp <=122) {
                level++;
                break;
            }
        }
        
        //Check Uppercase
        for (int i=0;i<input.length();i++){
            char temp = input.charAt(i);
            if (temp >= 65 && temp <=90) {
                level++;
                break;
            }
        }


        System.out.println("Your Password level is : " + level);
    }

    public static void numberFive(Scanner scan) {
        System.out.println("===SOAL SIMULASI 06===");
        System.out.print("Masukkan kalimat : ");
        scan.nextLine();
        String input = scan.nextLine().toLowerCase();
        input = input.replaceAll("[\\W_]", "");
        char[] vokal = charArry(input, "vokal");
        char[] konsonan = charArry(input, "konsonan");

        System.out.println(Arrays.toString(vokal));
        // System.out.println(Arrays.toString(konsonan));
        
    }

    public static void numberSix(Scanner scan) {
        System.out.println("===SOAL SIMULASI 06===");
        System.out.print("Masukkan maksimal deret : ");
        int maksDeret = scan.nextInt();
        int[] deret= deret(maksDeret);
        System.out.print("\nGenap\t: ");
        checkDeret(deret,"GENAP");
        System.out.print("\nGanjil\t: ");
        checkDeret(deret,"GANJIL");
        System.out.println();
    }

    public static void numberSeven(Scanner scan) {
        System.out.println("===SOAL SIMULASI 07===");
        System.out.println("Masukkan U untuk berapa kali naik gunung, D untuk berapa kali turun lembah :");
        scan.nextLine();
        String check = scan.nextLine().toUpperCase();
        System.out.println(checkUD(check));
    }

    public static void numberEight(Scanner scan) {
        int uang = 500000;
        String check = "password";
        System.out.println("===SIMULASI NO 08===OVI Bank====");
        System.out.println("Uang anda adalah " + uang);
        System.out.print("Masukkan password : ");
        scan.nextLine();
        String password = scan.nextLine();
        if (password != check) {
            System.out.println("Password yang anda ketikkan salah!");
            return;
        }
        System.out.print("\nMasukkan nomer yang dituju : ");
        String nomer = scan.nextLine();
        System.out.print("\nMasukkan nominal uang yang akan dikirim : ");
        int sendMon = scan.nextInt();

        System.out.println("Anda akan mengirimkan uang sebesar " + sendMon + " ke " + nomer);
        System.out.print("\nApakah anda setuju ? (Y/N)");
        scan.nextLine();
        String conf = scan.nextLine().toUpperCase();
        if (conf == "Y"){
            System.out.print("\nMasukkan password anda : ");
            String pass = scan.nextLine();
            if (pass == check){
                if (uang < sendMon){
                    System.out.println("Maaf, uang yang anda miliki tidak cukup!");
                }
                uang -= sendMon;
                System.out.println("Sukses! uang anda sekarang menjadi " + uang);
            }
        }else return;
    }

    //method

    public static int randomNum(){
        Random acak = new Random();
        int random = acak.nextInt(1000);
        System.out.println("Angka random kali ini adalah : " + random);
        return random;
    }

    public static String checkNum(int num1, int num2, int num3){
        String check = "";
        if (num1 > num2 && num1 > num3) {
            check = "Angka paling besar adalah " + num1;
        } else if (num2 > num1 && num2 > num3) {
            check = "Angka paling besar adalah " + num2;
        } else if (num3 > num1 && num3 > num2) {
            check = "Angka paling besar adalah " + num3;
        }
        return check;
    }

    public static String checkUD(String string){
        String check = "";
        int checkU = 0, checkD = 0;
        for (int i=0;i<string.length();i++){
            char temp = string.charAt(i);
            if (temp == 'U') checkU++;
            else if (temp == 'D') checkD++;
        }   
        check = "Gunung yang dilewati sebanyak " + checkU + " dan lembah yang dilewati sebanyak " + checkD;
        return check;
    }

    public static int[] deret(int maksDeret){
        int[] urutanDeret = new int[maksDeret];
        int mulai = 1;
        for (int i=0; i<maksDeret;i++) {
            urutanDeret[i] = mulai;
            mulai++;
        }
        
        return urutanDeret;
    }

    public static void checkDeret(int[] deret, String pemilih){
        for (int i=0;i<deret.length;i++){
            int temp = deret[i];
            if (pemilih == "GENAP"){
                if (temp % 2 == 0) System.out.print(temp + "\t");
            } else if (pemilih == "GANJIL"){
                if (temp % 2 != 0) System.out.print(temp + "\t");
            }
        }
    }

    public static char[] charArry(String input, String choice) {
        int count = 0;
        for (int i=0;i<input.length();i++){
            char temp = input.charAt(i);
            if (choice ==  "vokal"){
                if (temp == 'a' || temp == 'i' || temp == 'u' || temp == 'e' || temp == 'o'){
                    count++;
                }
            }else if (choice == "konsonan"){
                if (temp != 'a' || temp != 'i' || temp != 'u' || temp != 'e' || temp != 'o'){
                    count++;
                }
            }
        }

        char[] charArryAlpha = new char[count];
        for (int i=0;i<count;i++){
            for (int j=0;j<input.length();j++){
            char temp = input.charAt(j);
            if (choice ==  "vokal"){
                if (temp == 'a' || temp == 'i' || temp == 'u' || temp == 'e' || temp == 'o'){
                    charArryAlpha[i]=temp;
                    break;
                    }
            }else if (choice == "konsonan"){
                if (temp != 'a' || temp != 'i' || temp != 'u' || temp != 'e' || temp != 'o'){
                    charArryAlpha[i]=temp;
                    }
                }
            }
        }

        return charArryAlpha;

    }

    public static char[] characterArrayAsc(char[] urutHuruf) {
        char swap = 0;
        // loop ke 1 untuk loop sebanyak item di array
        for (int i=0; i<urutHuruf.length;i++){
            //loop ke 2 untuk melakukan swap
            //j = 0  ascending
            //j = 1+i = descending
            for (int j=0; j < urutHuruf.length;j++){
                char a = urutHuruf[i];
                char b = urutHuruf[j];
                if (a<b){
                    swap = a;
                    urutHuruf[i] = urutHuruf[j];
                    urutHuruf[j] = swap;
                }
            }
        }
        return urutHuruf;
    }

    public static char[] characterArrayDesc(char[] urutHuruf) {
        char swap = 0;
        // loop ke 1 untuk loop sebanyak item di array
        for (int i=0; i<urutHuruf.length;i++){
            //loop ke 2 untuk melakukan swap
            //j = 0  ascending
            //j = 1+i = descending
            for (int j=1+i; j < urutHuruf.length;j++){
                char a = urutHuruf[i];
                char b = urutHuruf[j];
                if (a<b){
                    swap = a;
                    urutHuruf[i] = urutHuruf[j];
                    urutHuruf[j] = swap;
                }
            }
        }
        return urutHuruf;
    }

    public static void printDeret(char[] deret){
        for (int i=0;i<deret.length;i++){
            char temp = deret[i];
            System.out.print(temp + "\t");
        }
    }

}
