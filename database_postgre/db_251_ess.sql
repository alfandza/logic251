create database db_251_ess2
	with
	owner postgres
	encoding 'UTF8'
	connection limit = -1
	
DROP TABLE IF EXISTS biodata;
CREATE TABLE biodata (
	id bigint,
	first_name varchar(20), 
	last_name varchar(30), 
	dob varchar(10), 
	pob varchar(50), 
	address varchar(255)
);

INSERT INTO biodata VALUES (1, 'Raya', 'Indriyani', '1991-01-01', 'Bali', 'Jl. Raya Baru, Bali');
INSERT INTO biodata VALUES (2, 'Rere', 'Wulandari', '1992-01-02', 'Bandung', 'Jl. Berkah Ramadhan, Bandung');
INSERT INTO biodata VALUES (3, 'Bunga', 'Maria', '1991-03-03', 'Jakarta', 'Jl. Mawar Semerbak, Bogor');

DROP TABLE IF EXISTS employee;
CREATE TABLE employee (
	id bigint,
	biodata_id bigint,
	nip varchar(5), 
	status varchar(10), 
	salary decimal(10, 0)
);

INSERT INTO employee VALUES (1, 1, 'NX001', 'Permanen', 12000000);
INSERT INTO employee VALUES (2, 2, 'NX002', 'Kontrak', 15000000);

DROP TABLE IF EXISTS contact_person;
CREATE TABLE contact_person (
	id bigint,
	biodata_id bigint,
	type varchar(5), 
	contact varchar(100)
);

INSERT INTO contact_person VALUES (1, 1, 'MAIL', 'raya.indriyani@gmail.com');
INSERT INTO contact_person VALUES (2, 1, 'PHONE', '085612345678');
INSERT INTO contact_person VALUES (3, 2, 'MAIL', 'rere.wulandari@gmail.com');
INSERT INTO contact_person VALUES (4, 2, 'PHONE', '081312345678');
INSERT INTO contact_person VALUES (5, 2, 'PHONE', '087812345678');
INSERT INTO contact_person VALUES (6, 3, 'MAIL', 'bunga.maria@gmail.com');

DROP TABLE IF EXISTS leave;
CREATE TABLE leave (
	id bigint,
	type varchar(10), 
	name varchar(100)
);

INSERT INTO leave VALUES (1, 'Regular', 'Cuti Tahunan');
INSERT INTO leave VALUES (2, 'Khusus', 'Cuti Menikah');
INSERT INTO leave VALUES (3, 'Khusus', 'Cuti Haji & Umroh');
INSERT INTO leave VALUES (4, 'Khusus', 'Melahirkan');

DROP TABLE IF EXISTS employee_leave;
CREATE TABLE employee_leave (
  id integer,
  employee_id integer,
  period varchar(4),
  regular_quota integer
);

INSERT INTO employee_leave VALUES (1, 1, '2020', 16);
INSERT INTO employee_leave VALUES (2, 2, '2020', 12);

DROP TABLE IF EXISTS leave_request;
CREATE TABLE leave_request (
	id bigint,
	employee_id bigint,
	leave_id bigint,
	start_date date,
	end_date date,
	reason varchar(255)
);

INSERT INTO leave_request VALUES (1, 1, 1, '2020-10-10', '12-10-2020', 'Liburan');
INSERT INTO leave_request VALUES (2, 1, 1, '2020-11-12', '2020-11-15', 'Acara keluarga');
INSERT INTO leave_request VALUES (3, 2, 2, '2020-05-05', '2020-05-07', 'Menikah');
INSERT INTO leave_request VALUES (4, 2, 1, '2020-09-09', '2020-09-13', 'Touring');
INSERT INTO leave_request VALUES (5, 2, 1, '2020-12-24', '2020-12-25', 'Acara keluarga');

DROP TABLE IF EXISTS travel_type;
CREATE TABLE travel_type (
	id bigint,
	name varchar(50),
	travel_fee integer
);

INSERT INTO travel_type VALUES (1, 'In Indonesia', 200000);
INSERT INTO travel_type VALUES (2, 'Out Indonesia', 350000);

DROP TABLE IF EXISTS travel_request;
CREATE TABLE travel_request (
	id bigint,
	employee_id bigint,
	travel_type_id bigint,
	start_date date,
	end_date date
);

INSERT INTO travel_request VALUES (1, 1, 1, '2020-07-07', '2020-07-08');
INSERT INTO travel_request VALUES (2, 1, 1, '2020-08-07', '2020-08-08');
INSERT INTO travel_request VALUES (3, 2, 2, '2020-04-04', '2020-04-07');

DROP TABLE IF EXISTS travel_settlement;
CREATE TABLE travel_settlement (
	id bigint,
	travel_request_id bigint,
	item_name varchar(100),
	item_cost integer
);

INSERT INTO travel_settlement VALUES (1, 1, 'Tiket travel Do-Car berangkat', 165000);
INSERT INTO travel_settlement VALUES (2, 1, 'Hotel', 750000);
INSERT INTO travel_settlement VALUES (3, 1, 'Tiket travel Do-Car pulang', 165000);
INSERT INTO travel_settlement VALUES (4, 2, 'Tiket pesawat berangkat', 750000);
INSERT INTO travel_settlement VALUES (5, 2, 'Hotel', 650000);
INSERT INTO travel_settlement VALUES (6, 2, 'Tiket pesawat pulang', 1250000);
INSERT INTO travel_settlement VALUES (7, 3, 'Tiket pesawat berangkat', 4750000);
INSERT INTO travel_settlement VALUES (8, 3, 'Hotel', 6000000);
INSERT INTO travel_settlement VALUES (9, 2, 'Tiket pesawat pulang', 4250000);


--SOAL 1 mengurutkan data karyawan berdasarkan dob tertua ke termuda

select	 	e.nip,
			e.status,
			concat(b.first_name,' ',b.last_name) as nama_lengkap,
			b.dob,
			b.pob,
			b.address
from 		employee e
inner join 	biodata b on e.biodata_id = b.id
order by dob asc


--SOAL 2 mencari jumlah karyawan tetap dan kontrak

select 		emp.status,
			count(emp.status) as jumlah_karyawan
from 		employee emp
group by 	emp.status
order by	status


--SOAL 3 menampilkan yang bukan karyawan

--Left Join

select 		concat(b.first_name,' ', b.last_name) as nama_lengkap,
			b.dob,
			b.pob,
			b.address,
			e.status
from 		biodata b
left join 	employee e on b.id = e.biodata_id
where 		coalesce(e.status, '') not in ('Permanen','Kontrak')

--kalo bandingin satu bisa !=, kalo banyak pake not in

--Full outer join

select 				concat(b.first_name,' ', b.last_name) as nama_lengkap,
					b.dob,
					b.pob,
					b.address,
					e.status
from 				biodata b
full outer join 	employee e on b.id = e.biodata_id
where 				coalesce(e.status, '') not in ('Permanen','Kontrak')


--SOAL 04 menampilkan data karyawan yang permanen termasuk gaji, gajinya ditambah 5000000

select  	concat(bio.first_name,' ',bio.last_name) as nama_lengkap,
			bio.dob,
			emp.status,
			emp.salary,
			emp.salary+5000000 as salary_up
from 		employee emp
left join	biodata bio on emp.biodata_id = bio.id
where 		status = 'Permanen'


--soal lima menambah primary key di kolom id tabel biodata

select * from biodata

alter table biodata add primary key (id)

select * from biodata

--soal enam
/* Tampilkan nama karyawan, jenis perjalanan dinas, tanggal perjalanan dinas, 
dan total pengeluarannya selama perjalanan dinas tersebut */

select 		emp.nip,
			emp.status,
			concat(bio.first_name,' ',bio.last_name) as nama_lengkap,
			ttype.name as tipe_perjalanan,
			treq.start_date as awal_perjalanan,
			treq.end_date as akhir_perjalanan,
			sum(ts.item_cost)+sum(ttype.travel_fee) as total
from 		employee emp

inner join 	biodata bio
on emp.biodata_id = bio.id

inner join 	travel_request treq
on emp.id = treq.employee_id 

inner join travel_type ttype
on treq.travel_type_id = ttype.id

inner join travel_settlement ts
on treq.id = ts.travel_request_id 
group by emp.nip, emp.status,nama_lengkap,ttype.name,treq.start_date,treq.end_date
order by treq.start_date

-----------------------------------------------------------------------------------
---DIBAWAH INI CARA NGERJAIN YANG BENER

select 		t2.first_name || ' ' || t2.last_name as nama_lengkap,
			concat(t2.first_name,' ',t2.last_name) as nama_lengkap2,
			t1.nip,
			t1.status,
			t3.start_date,
			t3.end_date,
			t4.name,
			sum(t5.item_cost) as total
from 		employee t1
			inner join 	biodata 			t2 on t1.biodata_id = t2.id
			inner join	travel_request 		t3 on t1.id = t3.employee_id
			inner join	travel_type 		t4 on t3.travel_type_id = t4.id
			inner join	travel_settlement 	t5 on t3.id = t5.travel_request_id
group by	nama_lengkap, nama_lengkap2, t1.nip, t1.status,t3.start_date,t3.end_date,t4.name
			

--soal 7
/* Buatkan query untuk menampilkan data karyawan yang 
belum pernah melakukan perjalanan dinas*/

--bikin data karyawan baru yang belum melakukan perjalanan dinas
--INSERT INTO biodata VALUES (4, 'Cintya', 'Anjani', '1991-03-10', 'Jakarta', 'Jl. Mawar Semerbak, Jakarta');
--INSERT INTO employee VALUES (3, 4, 'NX003', 'Permanen', 19000000);

select		t1.nip,
			concat(t2.first_name, ' ',t2.last_name) as nama_lengkap,
			t1.status,
			t3.travel_type_id
			
from		employee t1
			left join biodata 			t2 on t1.biodata_id = t2.id
			left join travel_request 	t3 on t1.id = t3.employee_id
where		t3.travel_type_id is null

--soal 8
/* Tampilkan nama lengkap karyawan, jenis cuti, alasan cuti, durasi cuti, dan nomor 
telepon yang bisa dihubungi untuk masing-masing karyawan yang mengajukan cuti*/

--------------------------------------------------------------------------
---------DIBAWAH INI CARA MENGERJAKAN YANG BENAR

select		concat(t2.first_name,' ',t2.last_name) as nama_lengkap,
			t4.type,
			t4.name as jenis_cuti,
			t3.reason,
			t3.start_date,
			t3.end_date,
			t3.end_date - t3.start_date as durasi_cuti,
			t5.contact 
from		employee t1
			left join biodata 			t2 on t1.biodata_id = t2.id  
			left join leave_request 	t3 on t1.id 		= t3.employee_id
			left join leave 			t4 on t3.leave_id  	= t4.id
			left join (select 		distinct on (cp.biodata_id) biodata_id,
									cp.type,
									cp.contact
						from 		contact_person cp 
						where		cp.type = 'PHONE') t5 on t2.id			= t5.biodata_id 
where		t3.leave_id is not null



--Soal 9

/* Tampilkan alasan cuti yang paling sering diajukan karyawan*/

--inner join

select 		t2.reason as alasan_cuti,
			count(t2.reason) as paling_banyak
from 		employee t1 
			inner join leave_request 	t2 on t1.id = t2.employee_id 
group by 	t2.reason

--left join
select 		t2.reason as alasan_cuti,
			count(t2.reason) as paling_banyak
from 		employee t1 
			left join leave_request 	t2 on t1.id = t2.employee_id
where 		(t2.reason is not null or coalesce(t2.reason,'') != '')
group by 	t2.reason
order by 	count(t2.reason) desc limit 1

---cara lain

select 	reason,
		count(reason) as jumlah
from 	leave_request
group by reason 
order by jumlah desc limit 1

--SOAL 10

alter table leave add column quota int;

insert into leave (id,type,name,quota) values (5,'Regular','Cuti Harian',5);

update leave set quota = 3 where quota is null;

select * from leave;



