package com.xa.day1;

public class javaVariableStatic {
    static int sisi = 5; //Satuan cm
    
    public static void main(String[] args) {
        luasPersegi();
        kelilingPersegi();
    }

    public static void luasPersegi() {
        System.out.println("Luas before : " + sisi*sisi + " cm");
        sisi = 10;
        System.out.println("Luas after : " + sisi*sisi + " cm");
    }

    public static void kelilingPersegi() {
        System.out.println("Keliling : " + sisi*4 + " cm");
    }
}
