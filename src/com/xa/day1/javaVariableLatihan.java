package com.xa.day1;

public class javaVariableLatihan {
    static final String konstan = "60";
    public static void main(String[] args) {
        soalPertama();
        soalKedua();
        soalKetiga();
        soalKeempat();
    }

    public static void soalPertama() {
        int konstanta = Integer.parseInt(konstan);
        int soal1 = konstanta + 10 + 30;
        System.out.println("Jawaban dari soal 1 (\"60\" ditambah 10 ditambah 30) adalah : " + soal1);
    }

    public static void soalKedua() {
        int soal2 = 15000 * 4;
        System.out.println("Jawaban dari soal kedua (15000 dikali 4) adalah : " + soal2);
    }

    public static void soalKetiga() {
        int a=5,b=3;
        int soal3=(int)Math.pow(a, b);
        System.out.println("Jawaban dari soal ketiga (" + a + " pangkat " + b +") adalah : " + soal3);
    }

    public static void soalKeempat() {
        int soal4 = 10 % 5;
        System.out.println("Jawaban dari soal 4 (10 mod (%) 5) adalah : " + soal4);
    }

}
