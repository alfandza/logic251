package com.xa.day1;

public class javaOperator {
    public static void main(String[] args) {
        System.out.println("Postfix");
        opUnaryPostfix();
        System.out.println("Prefix");
        opUnaryPrefix();
        System.out.println("Arithmetic");
        opArithmetic();
        System.out.println("Assignment");
        opAssignment();
        System.out.println("Comparision");
        opComparision();
    }
    
    public static void opUnaryPostfix() {
        // Postfix
        int a = 0;
        System.out.println("Before : " + a);
        a++;
        System.out.println("After : " + a);

        int b = 0;
        System.out.println("Before : " + b);
        b--;
        System.out.println("After : " + b);
    }

    public static void opUnaryPrefix() {
        // Prefix
        int c = 0;
        System.out.println("Before : " + c);
        ++c;
        System.out.println("After : " + c);

        int d = 0;
        System.out.println("Before : " + d);
        --d;
        System.out.println("After : " + d);        
    }


    public static void opArithmetic() {
        int a = 10;
        int b = 2;
        int c;

        System.out.println("a = " + a + " b = " + b);

        //process +
        c = a+b;
        System.out.println("Arithmetic (+) : " + c);

        //process -
        c = a-b;
        System.out.println("Arithmetic (-) : " + c);

        //process *
        c = a*b;
        System.out.println("Arithmetic (*) : " + c);

        //process /
        c = a/b;
        System.out.println("Arithmetic (/) : " + c);

        //process %
        c = a%b;
        System.out.println("Arithmetic (%) : " + c);
    }

    public static void opAssignment() {
        // deklarasi variabel
        int x;

        // (=)
        x = 5;
        System.out.println("(=)" + x);

        // (+=)
        System.out.println("(=)" + (x+=5));
        System.out.println("(+=)" + (x-=5));
        System.out.println("(-=)" + (x*=1));
        System.out.println("(/=)" + (x/=1));
        System.out.println("(%=)" + (x%=1));
        System.out.println("(^=)" + (x^=50));
        System.out.println("(&=)" + (x&=0));
        System.out.println("(|=)" + (x|=1));
        System.out.println("(>>=)" + (x>>=3));
        System.out.println("(<<=)" + (x<<=3));
    }

    public static void opComparision() {
        int a = 5;
        int b = 10;
        System.out.println("a = " + a + " b = " + b);
        System.out.println("(==) " + (a==b));
        System.out.println("(!=) " + (a!=b));
        System.out.println("(>=) " + (a>=b));
        System.out.println("(<=) " + (a<=b));
        System.out.println("(>) " + (a>b));
        System.out.println("(<) " + (a<b));
    }
}

