package com.xa.extra.SoalHari10.Soal4OOP;

public class Rental {
    private String film;

    Rental(String film){
        this.setFilm(film);

    }

    public String getFilm() {
        return film;
    }

    public void setFilm(String film) {
        this.film = film;
    }

}
