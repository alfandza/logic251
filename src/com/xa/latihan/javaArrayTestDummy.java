package com.xa.latihan;

import java.util.Scanner;

public class javaArrayTestDummy {
    
    public static void main(String[] args) {
        for (int i = 0; i >= 0; i--) {
            System.out.println(i); 
        }
        soalLima();
        
        // Scanner scanner = new Scanner(System.in);
        // String replay = "Y";
        // do {
        //     soalSatu(scanner);
        //     System.out.println("\n\nMau ulang program? (Y/N)");
        //     replay = scanner.next().toUpperCase();
        // }
        // while (replay == "Y");
    }

    public static void soalSatu(Scanner scanner) {
        System.out.println("Exam 01");
        System.out.println("Masukkan inputan");
        String string = scanner.nextLine();
        String[] stringArray = string.split(" ");

        for(String string2 : stringArray){
            int stringLength=string2.length();
            for (int i=0; i<stringLength; i++){
                if (i != 0 && i != stringLength-1){
                    System.out.print("*");
                }else {
                    char hasilSoal=string2.charAt(i);
                    System.out.print(hasilSoal +" ");
                }
            }
        }
        

    }

    public static void soalTest() {
        String soal="Tapi saya Tidak Sayang KAMU!!";
        System.out.println("\nSoal 02");
        System.out.println("Input : " + soal);
        System.out.print("Output : ");
        String[] strArry = soal.split(" ");
        System.out.println(strArry.length);
        // for (int i=0; i<soal.length(); i++) {

        // }
    }

    public static void soalTestJuga() {
        String input="Aku Sayang Kamu";
        System.out.println("\nSoal 06");
        System.out.println("Input : " + input);
        System.out.print("Output : ");
        String[] stringArray = input.split(" ");
        System.out.println(stringArray.length);
    }

    public static void soalTestJugaDua() {
        String input = "abuba";
        System.out.println("\nSoal 08");
        System.out.println("Input : " + input);
        String[] inputArray = input.split(" ");
        System.out.println("Panjang array adalah " + inputArray.length);
        System.out.print("Output : ");
        String newKata = "";
        for (int i=inputArray.length-1; i>=0; i--) {
            int panjangKata = inputArray[i].length();
            for (int j=panjangKata-1; j>=0; j--) {
                char temp = inputArray[i].charAt(j);
                temp = Character.toUpperCase(temp);
                newKata += Character.toString(temp);
            }
            System.out.println(newKata);
        }
    }
    
    public static void soalLima() {
        String input = "habis makan kenyang";
        System.out.println("\nSoal 05");
        System.out.println("Input : " + input);
        System.out.print("Output : ");
        String[] inputArray = input.split(" ");

        for (int i = 0; i < inputArray.length; i++){
            String newKata="";
            int panjangKata= inputArray[i].length();
            int hurufTengah= panjangKata/2;
            for (int j=0; j<panjangKata; j++){
                char temp = inputArray[i].charAt(j);
                if (j == hurufTengah) {
                    temp = Character.toUpperCase(temp);
                    newKata += Character.toString(temp);
                } else if (j == hurufTengah+1) {
                    newKata += "***";
                } else continue;
            }
            System.out.print(newKata + " ");
        }

    }

}

// dummy soal no 1
//     String soal="Aku Sayang Kamu";
    //     char[] soalchar=new char[soal.length()];
    //     for (int i=0; i<soal.length(); i++) {
    //         if (i == 1 || i >= 5 && i <=8 || i>=12 && i<=13) {
    //             soalchar[i] = '*';
    //             continue;
    //         }
    //         soalchar[i] = soal.charAt(i);
    //     }

    //     for (char soalhuruf : soalchar) {
    //         System.out.print(soalhuruf);
    //     }
    // }
    // Cara kedua
    // String[] stringArray = string.split(" "); 
  
    //     for (String string2 : stringArray) 
    //         System.out.println(string2);
    
    //     char[] charArray= new char[stringArray[0].length()];
    //     char[] charArray1= new char[stringArray[1].length()];
    //     char[] charArray2= new char[stringArray[1].length()];
        
    //     for (int i=0; i<stringArray[0].length();i++) {
    //         if (i == 0 || i == stringArray[0].length()-1){
    //             charArray[i] = stringArray[0].charAt(i);
    //         } else {
    //             charArray[i] = '*';
    //         }
    //     }

    //     for (int i=0; i<stringArray[1].length();i++) {
    //         if (i == 0 || i == stringArray[1].length()-1){
    //             charArray1[i] = stringArray[1].charAt(i);
    //         } else {
    //             charArray1[i] = '*';
    //         }
    //     }

    //     for (int i=0; i<stringArray[2].length();i++) {
    //         if (i == 0 || i == stringArray[2].length()-1){
    //             charArray2[i] = stringArray[2].charAt(i);
    //         } else {
    //             charArray2[i] = '*';
    //         }
    //     }
    //     System.out.print(charArray);
    //     System.out.print(charArray1);
    //     System.out.print(charArray2);