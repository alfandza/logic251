package com.xa.latihan;

import java.util.*;

public class javaArrayTugas {
    // nomer 1, 3, 4, 5, 6, 10 sama method, ubah if
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        // soalSatu();
        // soalDua();
        // soalTiga();
        // soalEmpat();
        // soalLima();
        // soalEnam(scanner);
        // soalTujuh();
        // soalDelapan(scanner);
        soalSembilan();
        // soalSepuluh();
    }

    public static void soalSatu() {
        // Scanner scanner = new Scanner(System.in);
        // System.out.print("Masukkan kalimat : ");
        // String input = scanner.nextLine();

        String input = "Aku Sayang Kamu";
        System.out.println("Soal 01 (Sensor Tengah)");
        System.out.println("Input : " + input);
        System.out.print("Output : ");
        String[] strArry = input.split(" ");
        for (int i=0; i< strArry.length; i++) {
            String newKata = "";
            int panjangKata = strArry[i].length();
            for (int j=0; j<panjangKata; j++) {
                char temp = strArry[i].charAt(j);
                if (j != 0 && j != panjangKata-1){
                    newKata += "*";
                }
                else{
                    newKata += Character.toString(temp);
                }
                
            }
            System.out.print(newKata + " ");
        }
    }

    public static void soalDua() {
        String soal="Tapi saya Tidak Sayang KAMU!!";
        System.out.println("\nSoal 02 (Cari berapa banyak kapital)");
        System.out.println("Input : " + soal);
        System.out.print("Output : ");
        String[] strArry = soal.split(" ");
        String newKata = "";
        for (int i=0; i<strArry.length;i++) {
            int panjangKata  = strArry[i].length();
            for (int j=0; j<panjangKata; j++) {
                char temp = strArry[i].charAt(j);
                boolean test = Character.isUpperCase(temp);
                if (test == true) {
                    newKata += Character.toString(temp);
                } else continue;
            }
        }
        System.out.print(newKata.length());

        // Catatan dari lain buat lebih efisien
        /* 
        String kalimat = "Tapi saya Tidak Sayang KAMU!!";
        int upperCase = 0;

        for (int i=0;i<kalimat.length(); i++) {
            char huruf = kalimat.charAt(i);
            if (huruf >= 65 && huruf <= 90) {
                upperCase++
            }
        }
        System.out.println(upperCase);

        */
    }

    public static void soalTiga() {
        String soal = "aku mau makan";
        System.out.println("\nSoal 03 (Sensor selain depan, depan dikapital)");
        System.out.println("Input : " + soal);
        System.out.print("Output : ");
        String[] stringArray = soal.split(" ");
        for (int i=0; i<stringArray.length; i++) {
            String saveWord = "";
            int lengthWord = stringArray[i].length();
            for (int j=0; j<lengthWord; j++){
                char tempWord = stringArray[i].charAt(j);
                if (j != 0) {
                    saveWord += "*";
                } else {
                    tempWord = Character.toUpperCase(tempWord);
                    saveWord += Character.toString(tempWord);
                }
            }
            System.out.print(saveWord + " ");
        }

    }

    public static void soalEmpat() {
        String soal = "aku mau mandi";
        System.out.println("\nSoal 04 (Sensor selain belakang, belakang dikapital)");
        System.out.println("Input : " + soal);
        System.out.print("Output : ");
        String[] stringArray = soal.split(" ");
        for (int i=0; i<stringArray.length; i++) {
            String saveWord = "";
            int lengthWord = stringArray[i].length();
            for (int j=0; j<lengthWord; j++){
                char tempWord = stringArray[i].charAt(j);
                if (j != lengthWord-1) {
                    saveWord += "*";
                } else {
                    tempWord = Character.toUpperCase(tempWord);
                    saveWord += Character.toString(tempWord);
                }
            }
            System.out.print(saveWord + " ");
        }
    }
 
    public static void soalLima() {
        String input = "habis makan kenyang";
        System.out.println("\nSoal 05 (Ambil huruf tengah)");
        System.out.println("Input : " + input);
        System.out.print("Output : ");
        String[] inputArray = input.split(" ");

        for (int i = 0; i < inputArray.length; i++){
            String newKata="";
            int panjangKata= inputArray[i].length();
            int hurufTengah=panjangKata/2;
            for (int j=0; j<panjangKata; j++){
                char temp = inputArray[i].charAt(j);
                if (j == hurufTengah) {
                    temp = Character.toUpperCase(temp);
                    newKata += Character.toString(temp);
                } else if (j == hurufTengah+1) {
                    newKata += "***";
                } else continue;
            }
            System.out.print(newKata + " ");
        }

    }

    public static void soalEnam(Scanner scanner) {
        System.out.print("Masukkan kalimat : ");
        String soal6 = scanner.nextLine();
        // String soal6 = "saya pasti bisa";
        System.out.println("\nSoal 06");
        System.out.println("Input : " + soal6);
        System.out.print("Output : ");
        String[] stringArray = soal6.toUpperCase().split(" ");

        for (int a=0; a<stringArray.length;a++){
            String plusKata = stringArray[a].substring(2);
            System.out.print(plusKata + " ");
        }
        
    }

    public static void soalTujuh() {
        String input="PT. XSIS MITRA UTAMA";
        System.out.println("\nSoal 07");
        System.out.println("Input : " + input);
        System.out.print("Output : ");
        // input = input.replaceAll(" ", "");
        // System.out.println(input);
        int bukanAlphabet = 0;
        for (int i=0; i<input.length();i++) {
            char temp = input.charAt(i);            //Lihat ASCII Table
            if ( !(temp>=65 && temp<=90) &&         //A - Z = 65 - 90
                 !(temp>=97 && temp<=122) &&        //a - z = 97 - 122
                 !(temp==32) &&                     //Space = 32  
                 !(temp>=48 && temp<=57)) {         //0 - 9 = 48 - 57 
                bukanAlphabet++;    
            }
        }
        System.out.println(bukanAlphabet);
        // split string
        // String[] stringArray = input.split(" ");
        // String newKata = "";

        // for (int i=0;i<stringArray.length;i++) {
        //     int panjangKata = stringArray[i].length();
        //     for (int j=0;j<panjangKata;j++){
        //         char temp = stringArray[i].charAt(j);
        //         boolean test = Character.isUpperCase(temp);
        //         if (test == false){
        //             newKata += Character.toString(temp);
        //         } else continue;
        //     }
        // }
        // System.out.print(newKata.length());
    }

    public static void soalDelapan(Scanner scanner) {
        System.out.print("Masukkan kalimat : ");
        String input = scanner.nextLine();
        System.out.println("\nSoal 08 (Mirror)");
        System.out.println("Input : " + input);
        System.out.print("Output : ");
        // String[] inputArray = input.split(" ");
        String newKata = "";
        for (int i=input.length()- 1; i >= 0; i--) {
            char temp = input.charAt(i);
            newKata += Character.toString(temp);
        }
        System.out.print(newKata);
    }

    public static void soalSembilan() {
        String soal="ManTuL bingits";
        System.out.println("\nSoal 09 (Array huruf per kata)");
        System.out.println("Input : " + soal);
        System.out.print("Output : ");
        String[] soalArray = soal.toLowerCase().split(" ");
        
        for (String kata : soalArray) {
            char[] temp = kata.toCharArray();
            Arrays.sort(temp);
            System.out.print(new String(temp)+ " ");
        }

        // for (int i=0;i<soalArray.length;i++){
        //     char[] charArray=soalArray[i].toCharArray();
        //     Arrays.sort(charArray);
        //     String output = new String(charArray);
        //     System.out.print(output + " ");
        //     //System.out.print(Arrays.toString(charArray) + " ");
        // }
    }

    public static void soalSepuluh() {
        String soal10 = "kerupuk udang balado";
        System.out.println("\nSoal 10 (Ambil huruf awal dan akhir, tambah ***, selain itu continue)");
        System.out.println("Input : " + soal10);
        System.out.print("Output : ");

        String[] soalArray = soal10.split(" ");

        for (int i=0; i<soalArray.length;i++){
            String newKata = " ";
            int panjangKata = soalArray[i].length();
            for (int j=0; j<panjangKata; j++) {
                char temp = soalArray[i].charAt(j);
                if (j == 0 || j == panjangKata-1){
                    newKata += Character.toString(temp);
                } else if (j == 1){
                    newKata += "***";
                } else continue;

            }
            System.out.print(newKata + " ");
        }
    }

}
