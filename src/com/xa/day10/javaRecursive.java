package com.xa.day10;

import java.util.*;

public class javaRecursive {
    
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        soalSatu(scan);
    }

    public static void soalSatu(Scanner scan) {
        System.out.println("Recursive digit sum");
        
        System.out.print("Masukkan nilai pengulangan : ");
        int duplicate = scan.nextInt();
        
        System.out.print("Masukkan angka : ");
        scan.nextLine();
        String angkaStr = scan.nextLine();

        String tampungAngkaStr = "";
        for (int i=0; i<duplicate; i++){
            tampungAngkaStr += angkaStr;
        }
        System.out.println(recurtionAdd(tampungAngkaStr));
    }
    
    public static String recurtionAdd(String angka) {
        int temp = 0;
        for (int i=0;i<angka.length();i++){
            int angkatemp = Integer.parseInt(String.valueOf(angka.charAt(i)));
            temp += angkatemp;
        }
        // System.out.println(temp);
        if (temp > 9) {
            return recurtionAdd(String.valueOf(temp));
        }else{
            return String.valueOf(temp);
        }
    }

    
    // public static int sum(int nilai){
    //     if (nilai > 0){
    //         return nilai + sum(nilai-1));
    //     }
    //     else return 10;
    // }

}
