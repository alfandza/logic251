package com.xa.day3;

public class javaArrayLatihan {
    
    public static void main(String[] args) {
        latihanSoal();
    }

    public static void latihanSoal() {
        int [][] arraybanyak = { {1, 2, 3, 4, 5, 6, 7, 8}, 
                                 {9,10,11,12}, 
                                 {13,14,15,16,17}, 
                                 {18,19,20,21,22,23} };
        System.out.println("Nilai array pada row 0 col 6 adalah : " + arraybanyak[0][6]);
        System.out.println("Nilai array pada row 1 col 2 adalah : " + arraybanyak[1][2]);
        System.out.println("Nilai array pada row 2 col 3 adalah : " + arraybanyak[2][3]);
        System.out.println("Nilai array pada row 3 col 0 adalah : " + arraybanyak[3][0]);
        System.out.println("Nilai array pada row 3 col 1 adalah : " + arraybanyak[3][1]);
        System.out.println("Nilai array pada row 3 col 5 adalah : " + arraybanyak[3][5]);

        for (int i = 0; i<arraybanyak[0].length; i++){
            System.out.print(arraybanyak[0][i] + "\t");
        }
        System.out.println(" ");
        for (int i = 0; i<arraybanyak[1].length; i++){
            System.out.print(arraybanyak[1][i] + "\t");
        }
        System.out.println(" ");
        for (int i = 0; i<arraybanyak[2].length; i++){
            System.out.print(arraybanyak[2][i] + "\t");
        }
        System.out.println(" ");
        for (int i = 0; i<arraybanyak[3].length; i++){
            System.out.print(arraybanyak[3][i] + "\t");
        }
        System.out.println(" ");
    }

}
