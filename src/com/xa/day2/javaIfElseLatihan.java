package com.xa.day2;

import java.util.Scanner;

public class javaIfElseLatihan {
    static int maxlulus=100,lulus=60, remed1=59, remed2=55;
    public static void main(String[] args) {
        nilaiKKM();
    }

    public static void nilaiKKM(){
        int nilai;
        Scanner scanner = new Scanner(System.in);

        System.out.print("Input nilai : ");
        nilai = scanner.nextInt();

        if (nilai >= lulus && nilai <=maxlulus){
            System.out.println("Selamat! Anda dinyatakan lulus! Nilai anda " + nilai);
        } else if (nilai >= remed2 && nilai <= remed1){
            System.out.println("Mohon maaf, anda dinyatakan remidi! Nilai anda " + nilai);
        } else if (nilai < remed2) {
            System.out.println("Mohon maaf, anda dinyatakan gagal! Nilai anda " + nilai);
        } else {
            System.out.println("Mohon maaf, nilai yang anda masukkan salah!");
        }

        scanner.close();
    }

}
