package com.xa.day8;

class Bilangan1 {
    //attribute
    private int x;
    private int y;

    //constructor
    public Bilangan1(){
        this.x = 100;
        this.y = 200;
    }

    // method get set
    public int getX(){
        return this.x;
    }

    public void setX(int x1){
        this.x = x1;
    }

    public int getY(){
        return this.y;
    }

    public void setY(int y){
        this.y = y;
    }
}

public class Main18ConstructorOOP {
    
    public static void main(String[] args) {
        // membuat objek Bilangan1
        Bilangan1 bil1 = new Bilangan1();
        System.out.println("Nilai X pada Bilangan1 adalah : " + bil1.getX());
        System.out.println("Nilai Y pada Bilangan1 adalah : " + bil1.getY());
        bil1.setX(69);
        bil1.setY(96);
        System.out.println("Nilai X pada Bilangan1 adalah : " + bil1.getX());
        System.out.println("Nilai X pada Bilangan1 adalah : " + bil1.getY());
        
        
        //method setXY di dalam
        setXY(40,33);

        // //method get set didalam
        // System.out.println("Nilai X pada Bilangan1 adalah : " + getX(55));
    }
    
    public static void setXY(int x,int y){
        Bilangan1 bil1 = new Bilangan1();
        bil1.setX(x);bil1.setY(y);
        System.out.println("Nilai X Panggil method setXY di class main : " + bil1.getX());
        System.out.println("Nilai Y Panggil method setXY di class main : " + bil1.getY());
    }

    // //pemanggilan di method lain di class utama
    // public static void setX(int x){
    //     Bilangan1 bil1 = new Bilangan1();
    //     bil1.setX(x);
    //     System.out.println("Panggil method setX di class main : " + bil1.getX());
    // }

    // //pemanggilan pakai getX dalam method
    // public static int getX(int x){
    //     Bilangan1 bil1 = new Bilangan1();
    //     bil1.setX(x);
    //     return bil1.getX();
    // }

    // public static int getX(int x, Bilangan1 bil1){
    //     bil1.setX(x);
    //     return bil1.getX();
    // }

    // public static void getX(Bilangan1 bil1) {
    //     System.out.println("Cetak x di method getX = " + bil1.getX());
    // }
}
