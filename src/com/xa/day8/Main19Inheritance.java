package com.xa.day8;

public class Main19Inheritance {
    //deklarasi
    static Car myCar = new Car();
    
    public static void main(String[] args) {
        System.out.print("Suara klakson mobil saya adalah : ");
        myCar.klakSon();

        System.out.println("Merknya " + myCar.merk);
        System.out.println("Tipenya " + myCar.getNameModel());
    }


}

class Vehicle {
    // Attribute vehicle
    protected String merk = "Honda";

    //method
    public void klakSon() {
        System.out.println("MBERRRRRRRRRRRRR");
    }
}

class Car extends Vehicle {
    // Attribute car
    private String nameModel = "Civic";

    //method
    public String getNameModel() {
        return this.nameModel;
    }
}