package com.xa.day2;

public class javaWhileLoop {

    public static void main(String[] args) {
        //whileSatu();
        doWhileLoop();
    }

    static void whileSatu(){
        /*
        *   int i=0;
        *   while (condition) {
        *       // Code block to be executed
        * }
        *   cek dulu, baru lakuin
        * */

        int i = 1;
        while (i<=5) {
            System.out.println(i++);
        }

    }

    static void doWhileLoop(){
        /*
         *   int i=0;
         *   do {
         *       // Code block to be executed
         *   }
         *   while (condition)
         *
         *  lakuin dulu, baru cek kondisi
         * */
        int i=1;
        do {
            System.out.println(i);
            i++;
        } while (i<=5);
    }


}
