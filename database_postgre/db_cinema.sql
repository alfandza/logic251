CREATE TABLE tbl_m_artis (
	id_artis_pk serial PRIMARY key,
	kode_artis character varying(255) NULL,
	nama_artis character varying(255) NULL,
	jenis_kelamin character varying(255) NULL,
	bayaran integer NULL,
	award integer NULL,
	id_negara_fk integer NULL,
	is_active integer NULL,
	created_by character varying(255) NULL,
	created_date timestamp NULL,
	updated_by character varying(255) NULL,
	updated_date timestamp NULL
);

CREATE TABLE tbl_m_genre (
	id_genre_pk serial PRIMARY key,
	kode_genre character varying(255) NULL,
	nama_genre character varying(255) NULL,
	is_active integer NULL,
	created_by character varying(255) NULL,
	created_date timestamp NULL,
	updated_by character varying(255) NULL,
	updated_date timestamp NULL
);

CREATE TABLE tbl_m_negara (
	id_negara_pk serial PRIMARY key,
	kode_negara character varying(255) NULL,
	nama_negara character varying(255) NULL,
	is_active integer NULL,
	created_by character varying(255) NULL,
	created_date timestamp NULL,
	updated_by character varying(255) NULL,
	updated_date timestamp NULL
);

CREATE TABLE tbl_m_produser (
	id_produser_pk serial PRIMARY key,
	kode_produser character varying(255) NULL,
	nama_produser character varying(255) NULL,
	international character varying(255) NULL,
	is_active integer NULL,
	created_by character varying(255) NULL,
	created_date timestamp NULL,
	updated_by character varying(255) NULL,
	updated_date timestamp NULL
);

CREATE TABLE tbl_t_film(
	id_film_pk serial PRIMARY key,
	kode_film character varying(255) NULL,
	nama_film character varying(255) NULL,
	id_genre_fk integer NULL,
	id_artis_fk integer NULL,
	id_produser_fk integer NULL,
	pendapatan integer NULL,
	nominasi integer NULL,
	is_active integer NULL,
	created_by character varying(255) NULL,
	created_date timestamp NULL,
	updated_by character varying(255) NULL,
	updated_date timestamp NULL
) ;

INSERT INTO public.tbl_m_artis (kode_artis,nama_artis,jenis_kelamin,bayaran,award,id_negara_fk,is_active,created_by,created_date,updated_by,updated_date) VALUES ('A001','ROBERT DOWNEY JR','PRIA',10000,2,1,1,'adit',now(),'adit', now());
INSERT INTO public.tbl_m_artis (kode_artis, nama_artis, jenis_kelamin, bayaran, award, id_negara_fk, is_active, created_by, created_date, updated_by, updated_date) VALUES ( N'A001', N'ROBERT DOWNEY JR', N'PRIA', 1000000000, 2, 1, 1, N'adit', now(),'adit', now());
INSERT INTO public.tbl_m_artis (kode_artis, nama_artis, jenis_kelamin, bayaran, award, id_negara_fk, is_active, created_by, created_date, updated_by, updated_date) VALUES ( N'A002', N'ANGELINA JOLIE', N'WANITA', 700000000, 1, 1, 1, N'adit', now(),'adit', now());
INSERT INTO public.tbl_m_artis (kode_artis, nama_artis, jenis_kelamin, bayaran, award, id_negara_fk, is_active, created_by, created_date, updated_by, updated_date) VALUES ( N'A003', N'JACKIE CHAN', N'PRIA', 1200000000, 7, 2, 1, N'adit', now(),'adit', now());
INSERT INTO public.tbl_m_artis (kode_artis, nama_artis, jenis_kelamin, bayaran, award, id_negara_fk, is_active, created_by, created_date, updated_by, updated_date) VALUES ( N'A004', N'JOE TASLIM', N'PRIA', 350000000, 1, 3, 1, N'adit', now(),'adit', now());
INSERT INTO public.tbl_m_artis (kode_artis, nama_artis, jenis_kelamin, bayaran, award, id_negara_fk, is_active, created_by, created_date, updated_by, updated_date) VALUES ( N'A005', N'CHELSEA ISLAN', N'WANITA', 300000000, 0, 3, 1, N'adit', now(),'adit', now());
INSERT INTO public.tbl_m_genre (kode_genre, nama_genre, is_active, created_by, created_date, updated_by, updated_date) VALUES ( N'G001', N'ACTION', 1, N'adit', now(),'adit', now());
INSERT INTO public.tbl_m_genre (kode_genre, nama_genre, is_active, created_by, created_date, updated_by, updated_date) VALUES ( N'G002', N'HORROR', 1, N'adit', now(),'adit', now());
INSERT INTO public.tbl_m_genre (kode_genre, nama_genre, is_active, created_by, created_date, updated_by, updated_date) VALUES ( N'G003', N'COMEDY', 1, N'adit', now(),'adit', now());
INSERT INTO public.tbl_m_genre (kode_genre, nama_genre, is_active, created_by, created_date, updated_by, updated_date) VALUES ( N'G004', N'DRAMA', 1, N'adit', now(),'adit', now());
INSERT INTO public.tbl_m_genre (kode_genre, nama_genre, is_active, created_by, created_date, updated_by, updated_date) VALUES ( N'G005', N'THRILLER', 1, N'adit', now(),'adit', now());
INSERT INTO public.tbl_m_genre (kode_genre, nama_genre, is_active, created_by, created_date, updated_by, updated_date) VALUES ( N'G006', N'FICTION', 1, N'adit', now(),'adit', now());
INSERT INTO public.tbl_m_negara ( kode_negara, nama_negara, is_active, created_by, created_date, updated_by, updated_date) VALUES ( N'AS', N'AMERKA SERIKAT', 1, N'adit', now(),'adit', now());
INSERT INTO public.tbl_m_negara ( kode_negara, nama_negara, is_active, created_by, created_date, updated_by, updated_date) VALUES ( N'HK', N'HONG KONG', 1, N'adit', now(),'adit', now());
INSERT INTO public.tbl_m_negara ( kode_negara, nama_negara, is_active, created_by, created_date, updated_by, updated_date) VALUES ( N'ID', N'INDONESIA', 1, N'adit', now(),'adit', now());
INSERT INTO public.tbl_m_negara ( kode_negara, nama_negara, is_active, created_by, created_date, updated_by, updated_date) VALUES ( N'IN', N'INDIA', 1, N'adit', now(),'adit', now());
INSERT INTO public.tbl_m_produser ( kode_produser, nama_produser, international, is_active, created_by, created_date, updated_by, updated_date) VALUES ( N'PD01', N'MARVEL', N'YA', 1, N'adit', now(),'adit', now());
INSERT INTO public.tbl_m_produser ( kode_produser, nama_produser, international, is_active, created_by, created_date, updated_by, updated_date) VALUES ( N'PD02', N'HONG KONG CINEMA', N'YA', 1, N'adit', now(),'adit', now());
INSERT INTO public.tbl_m_produser ( kode_produser, nama_produser, international, is_active, created_by, created_date, updated_by, updated_date) VALUES ( N'PD03', N'RAPI FILM', N'TIDAK', 1, N'adit', now(),'adit', now());
INSERT INTO public.tbl_m_produser ( kode_produser, nama_produser, international, is_active, created_by, created_date, updated_by, updated_date) VALUES ( N'PD04', N'PARKIT FILM', N'TIDAK', 1, N'adit', now(),'adit', now());
INSERT INTO public.tbl_m_produser ( kode_produser, nama_produser, international, is_active, created_by, created_date, updated_by, updated_date) VALUES ( N'PD05', N'PARAMOUNT PICTURE', N'YA', 1, N'adit', now(),'adit', now());
INSERT INTO public.tbl_t_film ( kode_film, nama_film, id_genre_fk, id_artis_fk, id_produser_fk, pendapatan, nominasi, is_active, created_by, created_date, updated_by, updated_date) VALUES ( N'F001', N'IRON MAN', 1, 1, 1, 2000000000, 3, 1, N'adit', now(),'adit', now());
INSERT INTO public.tbl_t_film ( kode_film, nama_film, id_genre_fk, id_artis_fk, id_produser_fk, pendapatan, nominasi, is_active, created_by, created_date, updated_by, updated_date) VALUES ( N'F002', N'IRON MAN 2', 1, 1, 1, 1800000000, 2, 1, N'adit', now(),'adit', now());
INSERT INTO public.tbl_t_film ( kode_film, nama_film, id_genre_fk, id_artis_fk, id_produser_fk, pendapatan, nominasi, is_active, created_by, created_date, updated_by, updated_date) VALUES ( N'F003', N'IRON MAN 3', 1, 1, 1, 1200000000, 0, 1, N'adit', now(),'adit', now());
INSERT INTO public.tbl_t_film ( kode_film, nama_film, id_genre_fk, id_artis_fk, id_produser_fk, pendapatan, nominasi, is_active, created_by, created_date, updated_by, updated_date) VALUES ( N'F004', N'AVENGER : CIVIL WAR', 1, 1, 1, 2000000000, 1, 1, N'adit', now(),'adit', now());
INSERT INTO public.tbl_t_film ( kode_film, nama_film, id_genre_fk, id_artis_fk, id_produser_fk, pendapatan, nominasi, is_active, created_by, created_date, updated_by, updated_date) VALUES ( N'F005', N'SPIDERMAN HOME COMING', 1, 1, 1, 1300000000, 0, 1, N'adit', now(),'adit', now());
INSERT INTO public.tbl_t_film ( kode_film, nama_film, id_genre_fk, id_artis_fk, id_produser_fk, pendapatan, nominasi, is_active, created_by, created_date, updated_by, updated_date) VALUES ( N'F006', N'THE RAID', 1, 4, 3, 800000000, 5, 1, N'adit', now(),'adit', now());
INSERT INTO public.tbl_t_film ( kode_film, nama_film, id_genre_fk, id_artis_fk, id_produser_fk, pendapatan, nominasi, is_active, created_by, created_date, updated_by, updated_date) VALUES ( N'F007', N'FAST & FURIOUS', 1, 4, 5, 830000000, 2, 1, N'adit', now(),'adit', now());
INSERT INTO public.tbl_t_film ( kode_film, nama_film, id_genre_fk, id_artis_fk, id_produser_fk, pendapatan, nominasi, is_active, created_by, created_date, updated_by, updated_date) VALUES ( N'F008', N'HABIBIE DAN AINUN', 4, 5, 3, 670000000, 4, 1, N'adit', now(),'adit', now());
INSERT INTO public.tbl_t_film ( kode_film, nama_film, id_genre_fk, id_artis_fk, id_produser_fk, pendapatan, nominasi, is_active, created_by, created_date, updated_by, updated_date) VALUES ( N'F009', N'POLICE STORY', 1, 3, 2, 700000000, 3, 1, N'adit', now(),'adit', now());
INSERT INTO public.tbl_t_film ( kode_film, nama_film, id_genre_fk, id_artis_fk, id_produser_fk, pendapatan, nominasi, is_active, created_by, created_date, updated_by, updated_date) VALUES ( N'F010', N'POLICE STORY 2', 1, 3, 2, 710000000, 1, 1, N'adit', now(),'adit', now());
INSERT INTO public.tbl_t_film ( kode_film, nama_film, id_genre_fk, id_artis_fk, id_produser_fk, pendapatan, nominasi, is_active, created_by, created_date, updated_by, updated_date) VALUES ( N'F011', N'POLICE STORY 3', 1, 3, 2, 615000000, 0, 1, N'adit', now(),'adit', now());
INSERT INTO public.tbl_t_film ( kode_film, nama_film, id_genre_fk, id_artis_fk, id_produser_fk, pendapatan, nominasi, is_active, created_by, created_date, updated_by, updated_date) VALUES ( N'F012', N'RUSH HOUR', 3, 3, 5, 695000000, 2, 1, N'adit', now(),'adit', now());
INSERT INTO public.tbl_t_film ( kode_film, nama_film, id_genre_fk, id_artis_fk, id_produser_fk, pendapatan, nominasi, is_active, created_by, created_date, updated_by, updated_date) VALUES ( N'F013', N'KUNGFU PANDA', 3, 3, 5, 923000000, 5, 1, N'adit', now(),'adit', now());

--SOAL 1

create view vw_film as
select 		t1.nama_film,
			t2.nama_genre,
			t3.nama_artis,
			t4.nama_produser,
			t5.nama_negara
from		tbl_t_film t1
inner join	tbl_m_genre t2 on t1.id_genre_fk = t2.id_genre_pk
inner join	tbl_m_artis t3 on t1.id_artis_fk = t3.id_artis_pk
inner join	tbl_m_produser t4 on t1.id_produser_fk = t4.id_produser_pk
inner join 	tbl_m_negara t5 on t3.id_negara_fk = t5.id_negara_pk;

select * from vw_film;


--SOAL 2

select 		t1.nama_produser,
			t1.international,
			sum(t2.pendapatan) as pendapatan
from 		tbl_m_produser t1
left join 	tbl_t_film t2 on t1.id_produser_pk = t2.id_produser_fk 
where 		t1.international in ('TIDAK')
group by 	t1.nama_produser,t1.international
order by 	pendapatan

--SOAL 3

select 		t1.nama_produser,
			sum(t2.pendapatan) as pendapatan
from 		tbl_m_produser t1
left join 	tbl_t_film t2 on t1.id_produser_pk = t2.id_produser_fk 
where 		t1.nama_produser in ('MARVEL')
group by 	t1.nama_produser

--SOAL 4

select 		t1.nama_produser,
			sum(t2.pendapatan) as pendapatan
from 		tbl_m_produser t1
left join 	tbl_t_film t2 on t1.id_produser_pk = t2.id_produser_fk
group by 	t1.nama_produser
order by 	t1.nama_produser 

--SOAL 5

select 		t1.nama_produser,
			count(t2.nama_film) as jumlah_film
from 		tbl_m_produser t1
left join 	tbl_t_film t2 on t1.id_produser_pk = t2.id_produser_fk
group by 	t1.nama_produser
order by 	t1.nama_produser 

--SOAL 6

select 		t1.nama_film,
			t3.nama_artis
from		tbl_t_film t1
inner join	tbl_m_artis t3 on t1.id_artis_fk = t3.id_artis_pk
where 		t3.nama_artis ilike ('j%')

--SOAL 7
select 		t1.nama_film,
			t3.nama_artis,
			t4.nama_negara 
from		tbl_t_film t1
left join	tbl_m_artis t3 on t1.id_artis_fk = t3.id_artis_pk
left join	tbl_m_negara t4 on t3.id_negara_fk = t4.id_negara_pk 
where 		t4.nama_negara not ilike ('%O%')

--Soal 8

select 		t1.nama_film,
			t1.pendapatan 
from 		tbl_t_film t1
where		t1.nama_film ilike ('%o%')
order by 	t1.pendapatan desc limit 1

--soal 9

select 		t1.nama_film,
			t1.pendapatan 
from 		tbl_t_film t1
where		t1.nama_film ilike ('%o%')
order by 	t1.pendapatan asc limit 1

--soal 10
select 		avg(t1.pendapatan) as rata_rata
from 		tbl_t_film t1

--soal 11

alter table tbl_m_artis alter column created_date type timestamp;
alter table tbl_m_artis alter column updated_date type timestamp;

--soal 12


alter table tbl_m_artis alter column created_by type character varying(50);
alter table tbl_m_artis alter column updated_by type character varying(50);




