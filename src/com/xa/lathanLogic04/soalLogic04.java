package com.xa.lathanLogic04;

import java.util.*;

public class soalLogic04 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
       
        // System.out.println("Soal Nomer 1");
        // soalSatu(scanner);

        // System.out.println("Soal Nomer 2");
        // soalDua(scanner);
        
        System.out.println("Soal Nomer 3");
        soalTiga(scanner);
        
        // soalEmpat();
        
        // soalLima();
    }
    
    // Pengerjaan soal Satu
    public static void soalSatu(Scanner scanner) {
        System.out.print("Masukkan input : ");
        String camelCase = scanner.nextLine();
        ArrayList <String> camelCaseSeparator = new ArrayList<String>();
        //Need learning more
        for (String temp : camelCase.split("(?<=[a-z])(?=[A-Z])")) {
         camelCaseSeparator.add(temp);
        }
        System.out.println(camelCaseSeparator);
        System.out.println(camelCaseSeparator.size());
    }

    // Pengerjaan soal Dua
    public static void soalDua(Scanner scanner) {
        System.out.print("Masukkan input : ");
        String input = scanner.nextLine();
        System.out.print("Masukkan pergeseran alphabet : ");
        int alphaOffside = scanner.nextInt();
        System.out.println("\nInput : " + input);
        System.out.println("Alphabet offside : " + alphaOffside);
        // int alphaOffside=2;
        // String input = "middle-Outz";
        String output = "";
        for (int i=0; i<input.length();i++){
            char temp = input.charAt(i);
            boolean test = Character.isLetter(temp);
            for (int j=0;j<alphaOffside;j++){
                if (test == false) break;
                // if (temp > 64 && temp<91 && temp>96 && temp<123)
                if (temp == 90) {
                    temp = 65;
                    continue;
                }
                if (temp == 122) {
                    temp = 97;
                    continue;
                }
                temp++;
            }
            output += Character.toString(temp);
        }
        System.out.println("Output : "+output);
    }

    // Pengerjaan soal Tiga
    public static void soalTiga(Scanner scanner) {
        System.out.print("Masukkan input : ");
        String input = scanner.nextLine();
        String specialChar = "!@#S%^&*()-+";
        int level = 0;
        System.out.println("Soal Nomer 3");
        System.out.println("Input Password : " + input);
        
        //Char array for specialChar
        char[] specialCharArry = new char[specialChar.length()];
        for(int i=0;i<specialChar.length();i++) {
            specialCharArry[i] = specialChar.charAt(i);
        }

        
        //Check angka 
        for (int i=0;i<input.length();i++){
            char temp = input.charAt(i);
            if (temp >= 48 && temp <=57) {
                level++;
                break;
            }
        }
        
        //Check lowercase
        for (int i=0;i<input.length();i++){
            char temp = input.charAt(i);
            if (temp >= 97 && temp <=122) {
                level++;
                break;
            }
        }
        
        //Check Uppercase
        for (int i=0;i<input.length();i++){
            char temp = input.charAt(i);
            if (temp >= 65 && temp <=90) {
                level++;
                break;
            }
        }

        //Check Special Character
        for (int i=0;i<input.length();i++){
            char temp = input.charAt(i);
            if (temp >= specialCharArry[0] && temp <specialCharArry[11]) {
                level++;
                break;
            }
        }

        // System.out.println("Your Password level is : " + level);
        if (level == 1) {
            System.out.println("Your Password level is : " + level);
            System.out.println("Your password is too weak! please use uppercase,lowercase,number and special character!");
        } else if (level == 2) {
            System.out.println("Your Password level is : " + level);
            System.out.println("Your password is weak! please use uppercase,lowercase,number and special character!");
        } else if (level == 3) {
            System.out.println("Your Password level is : " + level);
            System.out.println("Your password is good");
        } else if (level == 4) {
            System.out.println("Your Password level is : " + level);
            System.out.println("Your password is strong!");
        } else System.out.println("Are you sure you input it right?");


    }


    // Pengerjaan soal Empat
    public static void soalEmpat() {
        String send = "SOSSPSS";
        int inputSignal = 4;
        String expect = "SOS";
        String expectedSignal = "";
        String differ = "x"; // Ada beda, keluar X
        String findDif = ""; 
        int countDif = 0;
        for (int i=0;i<inputSignal;i++){
            expectedSignal += expect;
        }
        System.out.println("Expected Signal : " + expectedSignal);
        System.out.println("Signal sended \t: " + send);

        //Case if sending too much
        if (send.length() > expectedSignal.length()){
            System.out.println("Too much signal! Please enter the right SOS as much as " + inputSignal + " times");
            return;
        }

        //Normal case
        System.out.print("Differences \t: ");
        for (int i=0; i<send.length();i++){
            char temp = send.charAt(i);
            char check = expectedSignal.charAt(i);
            if (check == temp){
                System.out.print(" ");
            } else {
                System.out.print(differ);
                countDif++;
            }
        }

        // Case if send too less than expectedSignal
        if (send.length() < expectedSignal.length()){
            int howMuch = expectedSignal.length() - send.length();
            for (int i=0;i<howMuch;i++){
                findDif += differ;
                countDif++;
            }
            System.out.print(findDif);
        }
        
        System.out.println("\nDifference Signal : " + countDif);
        
        // String expect = "SOS";
        // String expectedSignal = "";
        // int inputSignal = 4;
        // for (int i=0;i<inputSignal;i++){
        //     expectedSignal += expect;
        // }
        // String send = "SOSSPSSQSSOR";
        // System.out.println("Expected Signal : " + expectedSignal);
        // System.out.println("Signal sended \t: " + send);
        // System.out.print("Differences \t: ");
        // for (int i=0; i<expectedSignal.length();i++){
        //     char temp = expectedSignal.charAt(i);
        //     char check = send.charAt(i);
        //     if (check == temp){
        //         System.out.print(" ");
        //     } else System.out.print("x");
        // }
        // System.out.println();
    }

    // Pengerjaan soal Lima
    public static void soalLima() {
        String angka = "123456";
        System.out.println("Input : " + angka);
        ArrayList <String> angkaSeparator = new ArrayList<String>();
        //Need learning more
        for (String temp : angka.split("(?<=[0-9])")) {
         angkaSeparator.add(temp);
        }
        System.out.println("Output : " + angkaSeparator);
    }

}
