package com.xa.day8;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.Locale;

public class Main20DateAndTime {

    public static void main(String[] args) throws ParseException {
        LocalDate locDate = LocalDate.now(); //date atau tanggalan
        LocalTime locTime = LocalTime.now(); //time
        LocalDateTime locDateTime = LocalDateTime.now(); //date and time
        // Cara 1
        DateTimeFormatter dt = DateTimeFormatter.ofPattern("eeee, MMMM dd yyyy || h:mm:ss a");
        /* 
            e = hari
            M = bulan
            d = tanggal
            y = tahun
            H = Jam (24)
            h = Jam (12)
            a = AM/PM
        */
        String formatDT = dt.format(locDateTime);
        // end cara 1


        System.out.println("Date : " + locDate);
        System.out.println("Time : " + locTime);
        System.out.println("Date and Time : " + locDateTime);
        System.out.println("Date and Time Unformatted : " + dt);
        System.out.println("Date and Time Formatted : " + formatDT);

        //cara 2
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy", Locale.ENGLISH);
        Date dBefore = sdf.parse("01/01/2021");
        Date dAfter = sdf.parse("01/19/2021");

        System.out.println(dBefore + " | " + dAfter);

        // jarak hari
        LocalDate dateBefore = LocalDate.of(2021, 01, 01);
        long d = ChronoUnit.DAYS.between(dateBefore,locDate);
        System.out.println("Selisih hari dari " + dateBefore + " s/d " + locDate + " adalah : " + d);


    }

}
