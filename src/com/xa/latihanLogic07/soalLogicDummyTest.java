package com.xa.latihanLogic07;

import java.util.Arrays;

public class soalLogicDummyTest {
    public static void main(String[] args) {
        soalEnam();
        // soalTujuh();
    }

    public static void soalEnam(){
        int n = 7;
        int baris = 4;
        int nilai = 1;
        int total = 0;
        int[][] soalArray = new int[baris][n];
        boolean add = true;

        for (int i=0; i<baris;i++){
            
            for (int j=0; j<n;j++){
                total += nilai;
                if (add == true){
                    soalArray[i][j] = nilai++;
                } else soalArray[i][j] = nilai--;
            }

            nilai = total-1;
            total = 0;

            if (add) {
                add = false;
            } else add = true;
        }

        for (int i=0;i<baris;i++){
            System.out.println(Arrays.toString(soalArray[i]));
        }

    }

    public static void soalTujuh() {
        int awal0 = 0, awal1 = 1;
        int n = 9;
        int tengah = n/2;
        int[][] hasilArray = new int[2][n];

        for (int i=0; i<n;i++){
            hasilArray[0][i] = awal0;
            if (i >= tengah) awal0-=2;
            else awal0+=2;
        }

        for (int i=0; i<n;i++){
            hasilArray[1][i] = awal1;
            if (i >= tengah) awal1-=2;
            else awal1+=2;
        }

        for (int i=0;i<2;i++){
            System.out.println(Arrays.toString(hasilArray[i]));
        }
    }


}
