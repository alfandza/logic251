package com.xa.day1;

public class javaVariable {
    // declare instant variable
    // int b;
    // int c;
    
/*    public void biodata() {
        int nik = 251;
        String nama = "Muhammad Ekananda Alfandza Fajar";
        String ttl = "Kab. Semarang, 30-08-1999";
        String gender = "Pria";
        String alamat = "Jl. Urip Sumoharjo Perumahan Griya Sinar Mutiara Blok I2";        
    } */

/*    public int getC() { return c; }
    
    public void setC(int C) {
        this.c = c;
        // (kiri) variable baru / attribute
        // (kanan) nilai baru
    } */

    public static void main(String[] args) {
    // call instant var
    // javaVariable javVar = new javaVariable();
    // System.out.println(javVar.b);
    
    // local variable
    int nilai = 100;
    String nama = "Dodi";
    float nilaiFloat = 100.00f;
    nilaiFloat = 100f;
    boolean status = true;
    char gender = 'L';
    long nik = 325574812670001l;
    
    // output
    System.out.println("Int\t: " + nilai);
    System.out.println("String\t: " + nama);
    System.out.println("Float\t: " + nilaiFloat);
    System.out.println("Boolean\t: " + status);
    System.out.println("Char\t: " + gender);
    System.out.println("Long\t: " + nik);

    // String a = null;
    // System.out.println(a);
    learVariable();
    System.out.println(nama);
    hitungLuasPersegi();
    hitungKelilingPersegi();
    }
    
    public static void learVariable() {
        // tipe data + nama variable + nilai
        // cara pertama
        String nama = "M Ekananda A. F.";

        // cara kedua
        String gender;
        gender = "Laki-laki";

        String city;
        String address;

        // cara ketiga
        String name2, city2, address2;
        String  name3 = "Dindi", 
                city3 = "Jogja", 
                address3 = "Jl. Jembatan merah";
        
        nama = "Siti M. D.";

        System.out.println(nama);
    }

    public static void hitungLuasPersegi() {
        int sisi = 5; // Satuan cm

        // rumus luas persegi sisi x sisi
        int luas = sisi * sisi;
        System.out.println("Luas : " + luas + " cm");
        //System.out.println(sisi*sisi);
    }

    public static void hitungKelilingPersegi() {
        int sisi = 5;
         // rumus keliling
         int keliling = sisi * 4;
         System.out.println("Keliling : " + keliling + " cm");
    }

}
