package com.xa.day2;

public class javaWhileLoopLatihan {

    public static void main(String[] args) {
        //latihanSatu();
        latihanDua();
    }

    public static void latihanSatu(){
        int i = 0;
        while (i<10){
            i+=2;
            System.out.println(i);
        }

    }

    public static void latihanDua(){
        int i = 1, n=11;
        final int k=11;
        while (i<=5){
            System.out.println(n);
            n*=k;
            i++;
        }

    }

}
