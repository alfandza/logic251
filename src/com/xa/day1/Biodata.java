package com.xa.day1;

public class Biodata {

    public static void main(String[] args) {
       garisBiodata();
       identitasDiri();
    }

    private static void garisBiodata() {
        System.out.println("---------------------------------------------");
        System.out.println("------------------Biodata--------------------");
        System.out.println("---------------------------------------------");
    }

    private static void identitasDiri() {
        System.out.println("NIK              : " + 251);
        System.out.println("Nama             : Muhammad Ekananda Alfandza Fajar");
        System.out.println("Tempat/Tgl lahir : Kab. Semarang, " + 30 + "-08-" + 1999);
        System.out.println("Jenis Kelamin    : Pria");
        System.out.println("Alamat           : Jl. Urip Sumoharjo Perumahan Griya Sinar Mutiara Blok I2");
    } 

}